\documentclass[11pt,a4paper,onecolumn,titlepage,twoside]{scrartcl}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}

\newcommand{\theauthor}{\mbox{Steffen Brinkmann} }
\newcommand{\thetitle}{\mbox{CppSs - task-parallelism for C++} }
\author{\theauthor}
\title{\thetitle}

\newcommand{\btt}[1]{{\tt\bfseries #1}}
\newcommand{\figref}[1]{figure~\ref{#1}}
% use small capitals for Temanejo and Ayudame
\usepackage{xspace}
\newcommand{\tem}{\textsc{Temanejo}\xspace}
\newcommand{\ayu}{\textsc{Ayudame}\xspace}


\usepackage[usenames,dvipsnames]{color}
\definecolor{darkblue}{rgb}{.18,.19,.57}

\usepackage{fancyhdr}
\pagestyle{headings}
\usepackage{fancyvrb}
%\pagestyle{headings}
\pagestyle{fancy} %eigener Seitenstil
\fancyhf{} %alle Kopf- und Fusszeilenfelder bereinigen
\fancyhead[ER]{\leftmark}
\fancyhead[OL]{\rightmark}
\fancyhead[EL,OR]{\thepage}
\renewcommand{\headrulewidth}{0.4pt} %obere Trennlinie
\fancyfoot[EL]{\thetitle}
\fancyfoot[OR]{\theauthor}
\renewcommand{\footrulewidth}{0.4pt} %untere Trennlinie
\renewcommand{\sectionmark}[1]{
\markboth{\thesection{} #1}{}
}

\usepackage{hyperref}
\hypersetup{
pdfauthor = {Steffen Brinkmann},
pdftitle = {CppSs User Manual},
pdfsubject = {Manual for the CppSs library},
pdfkeywords = {CppSs, StarSs},
pdfcreator = {LaTeX with hyperref package},
pdfproducer = {dvips + ps2pdf},
colorlinks = {true},
urlcolor = {blue},
linktoc = all,
linkcolor = {darkblue}
}

\usepackage{listings}
\lstset{
	language=C++,
	basicstyle=\scriptsize\ttfamily,
	keywordstyle=\bfseries,
	captionpos=b,
	xleftmargin=5em,
	xrightmargin=5em,
	frame=single,
	framesep=1em,
	numbers=left,
	numberstyle=\textcolor{Gray},
	numbersep=1em,
	framexleftmargin=2em
%	framexrightmargin=10em
}

\sloppy

\begin{document}
\maketitle
\null
\vfill
\copyright \, 2012 Steffen Brinkmann $<$brinkmann@hlrs.de$>$\\[1em]

\footnotesize
This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 
Unported License. To view a copy of this license, visit 
\href{http://creativecommons.org/licenses/by-sa/3.0/}
{http://creativecommons.org/licenses/by-sa/3.0/} or send a letter to 
Creative Commons, 444 Castro Street, Suite 900, Mountain View, 
California, 94041, USA.
\normalsize
\cleardoublepage
%\pagebreak
\tableofcontents
\cleardoublepage

\section{Introduction}

CppSs \cite{2013CppSs} is a pure C++ library offering task-parallelism 
without the use of special compilers.

\section{Quick start}
\label{sec:quick}

Compile and install CppSs with

\begin{Verbatim}
$ autoreconf -fiv
$ ./configure --prefix=/install/path
$ make && make install-strip
\end{Verbatim}

A minimal program is listed in \figref{lst:minimal}. Compile and run it with

\begin{Verbatim}
$ g++ -std=c++0x -pthread ../test_cppss.c -o test_cppss -lcppss && ./test_cppss
\end{Verbatim}

The option \texttt{-pthread} might or might not be necessary on your system.
The library path of CppSs (e.g. /install/path/lib) must be added to the \texttt{LD\_LIBRARY\_PATH}
environment variable.

In order to use \tem \cite{Brinkmann2011} to debug the application use \texttt{-lcppss\_ayudame}.


\begin{figure}
\begin{lstlisting}

#include <CppSs.h>
#include <stdio.h>

void f_in(int *i) {
    printf("%d\n", *i);
}
CPPSS_TASKIFY(f_in,{IN})

void f_out(double* d) {
    *d = 1.3;
}
CPPSS_TASKIFY(f_out,{OUT})

void f_inout(double* d) {
    *d = *d * 2.0;
}
CPPSS_TASKIFY(f_inout,{INOUT})

void f_reduction(double *d) {
    *d = *d * 3.0;
}
CPPSS_TASKIFY(f_reduction,{REDUCTION})

void f_in_out(double* d, int* i) {
    *i = int(*d);
}
CPPSS_TASKIFY(f_in_out,{IN,OUT})

int main(void) {
    int i[1] = {1};
    double d[2] = {1.1,1.2};

    CppSs::Init(2,INFO);

    f_out_task(d);
    f_inout_task(d);
    f_reduction_task(d);
    f_in_out_task(d,i);
    CppSs::Barrier();//kind of obsolete here...
    f_in_task(i);

    CppSs::Finish();

    return 0;
}

\end{lstlisting}
\label{lst:minimal}
\caption{A minimal task-parallel program using CppSs.}
\end{figure}


\section{Install guide}

CppSs supports the GNU autotools. So the sequence of commands in 
section~\ref{sec:quick} should be sufficient to compile and install
CppSs.

\subsection{Prerequisites}

To compile CppSs, a C++ compiler covering some C++11 features is
necessary. Namely gcc version 4.6 and icc version 13 are capable
of compiling CppSs.

\section{User guide}

CppSs follows the StarSs \cite{starss-web} programming model.
In that sense it is quiet similar to SMPSs \cite{text-web}.

\subsection{Declare tasks}

Only functions can be converted to tasks. To do so, the programmer
has to call the function

\begin{Verbatim}
CppSs::MakeTask(...)
\end{Verbatim}

after the function is declared, but before it is first used.

It takes four arguments: The function pointer, an initializer list
with the directionality specifiers, the name of the function and
(optionally) the priority level.

\texttt{CppSs::MakeTask} returns a template class which is best declared
usiing the \texttt{auto} keyword.

For convenience CppSs provides the macros \texttt{CPPSS\_TASK}, 
\texttt{CPPSS\_TASK\_PRIORITY}, \texttt{CPPSS\_TASKIFY} and 
\texttt{CPPSS\_TASKIFY\_PRIORITY}. The former two wrap only the
function call to \texttt{MakeTask} and the functor has to (can be) named
freely when declared. The latter two wrap the whole functor declaration.
The resulting functor's name is composed by the original function name and 
the suffix \texttt{\_task}.

The three statements following the function declaration
translate into the same code:

\begin{lstlisting}
void func(int *a, double *b, float *c);
auto func_task = CppSs::MakeTask(func,
                                 {INOUT,IN,OUT},
                                 "func");
auto func_task = CPPSS_TASK(func,
                            {INOUT,IN,OUT});
CPPSS_TASKIFY(func,{INOUT,IN,OUT})
\end{lstlisting}


\subsection{Call tasks}

Before any task can be called, \texttt{CppSs::Init(n\_threads)} has
to be called. After the parallel region \texttt{CppSs::Finish()}
must be called.

\textbf{Attention:} No error is produced, if \texttt{CppSs::Init(n\_threads)}
is forgotten. The tasks are simply not scheduled!

Tasks are called as if they were normal functions. If the function
was declared a task using the \texttt{CPPSS\_TASKIFY} macro, the 
suffix \texttt{\_task} has to be added to the usual function call.

\section{Developer guide}

%\subsection{Design principles}

\subsection{Data types}

\begin{description}
\item[\btt{CppSs\_runtime::m\_all\_tasks}] is a \texttt{std::vector} which 
holds shared pointers (\texttt{shared\_ptr<Task\_instance\_base>}) to every 
task. The tasks are destroyed until the runtime is destroyed. The index is 
the task\_id, i.e. a pointer is accessed like this: 
\texttt{CppSs\_runtime::m\_all\_tasks[task\_id]}

\item[\btt{CppSs\_runtime::m\_last\_writers}] is a \texttt{std::map} whose 
key holds a represantation of a data pointer and whose value is the according 
task id. There must only be one task id for each data pointer (only one is 
the last one to write to the data), hence the choice of a map. Values can 
appear multiple times, as a task can be the last writer to a number of data.
\end{description}


\bibliographystyle{unsrt}
\bibliography{cppss_bibliography}

\end{document}

CppSs
=====

CppSs is an implementation of the task-based programming model. The
computational intesive functions are taskified and data dependencies are
defined. According to these dependencies a runtime system executes
the tasks asynchroniously.

CppSs stands in the tradition or StarSs programming models, see
http://www.bsc.es/computer-sciences/programming-models.

Usage
-----

A minimal program looks like this:

    
    #include <CppSs.h>
    #include <stdio.h>
    
    void f_in(int *i)
    {
        printf("%d\n", *i);
    }
    CPPSS_TASKIFY(f_in,{IN})
    
    void f_out(double* d)
    {
        *d = 1.3;
    }
    CPPSS_TASKIFY(f_out,{OUT})
    
    void f_inout(double* d)
    {
        *d = *d * 2.0;
    }
    CPPSS_TASKIFY(f_inout,{INOUT})
    
    void f_reduction(double *d)
    {
        *d = *d * 3.0;
    }
    CPPSS_TASKIFY(f_reduction,{REDUCTION})
    
    void f_in_out(double* d, int* i)
    {
        *i = int(*d);
    }
    CPPSS_TASKIFY(f_in_out,{IN,OUT})
    
    
    int main(void)
    {
    
        int i[1] = {1};
        double d[2] = {1.1,1.2};
    
        CppSs::Init(2,INFO);
    
        f_out_task(d);
        f_inout_task(d);
        f_reduction_task(d);
        f_in_out_task(d,i);
        CppSs::Barrier();//kind of obsolete here...
        f_in_task(i);
    
        CppSs::Finish();
    
        return 0;
    }

compile this with

    $ g++ -std=c++0x -pthread ../test_cppss.c -L/path/to/cppss/lib -lcppss
 
 Disclaimer
 ----------
 
 This code still has some undesired behaviour and is partly untested. Do not
 use in production environments.
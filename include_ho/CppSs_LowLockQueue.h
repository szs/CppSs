/*
 * CPPSs_LockFreeQueue.h
 *
 * header file for the CPPSs LockFreeQueue class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 * this code is based on Herb Sutters articles in Dr. Dobbs Journal
 * TODO: size
 *
 */


#ifndef LOCKFREEQUEUE_H
#define LOCKFREEQUEUE_H

#include "CppSs_defines.h"
#include "CppSs_includes.h"

// TODO: retrieve CACHE_LINE_SIZE from system
#define CACHE_LINE_SIZE 64
//#include <unistd.h>
//const long CACHE_LINE_SIZE=sysconf(_SC_LEVEL1_DCACHE_LINESIZE);


template <typename T>
struct LowLockQueue {
  const static std::size_t sizeof_Tp=sizeof(T*);
private:
  struct Node {
    const static std::size_t sizeof_Nodep=sizeof(std::atomic<Node*>);
    Node( T* val ) : value(val), next(nullptr) { }
    T* value;
    std::atomic<Node*> next;
    char pad[CACHE_LINE_SIZE - sizeof_Tp - sizeof_Nodep];
  };

  char pad0[CACHE_LINE_SIZE];

  // for one consumer at a time
  Node* first;

  char pad1[CACHE_LINE_SIZE - sizeof(Node*)];

  // shared among consumers
  std::atomic<bool> consumerLock;

  char pad2[CACHE_LINE_SIZE - sizeof(std::atomic<bool>)];

  // for one producer at a time
  Node* last;

  char pad3[CACHE_LINE_SIZE - sizeof(Node*)];

  // shared among producers
  std::atomic<bool> producerLock;

  char pad4[CACHE_LINE_SIZE - sizeof(std::atomic<bool>)];

public:
  LowLockQueue() {
    first = last = new Node( nullptr );
    producerLock = consumerLock = false;
  }

  ~LowLockQueue() {
    while( first != nullptr ) {	// release the list
      Node* tmp = first;
      first = tmp->next;
      delete tmp->value;	// no-op if null
      delete tmp;
    }
  }

  void add_task( const T& t ) {
    Node* tmp = new Node( new T(t) );
    while( producerLock.exchange(true) )
      { } 			// acquire exclusivity
    last->next = tmp;		// publish to consumers
    last = tmp;			// swing last forward
    producerLock = false;	// release exclusivity
  }


  bool get_task( T& result ) {
    while( consumerLock.exchange(true) )
      { }			// acquire exclusivity

    Node* theFirst = first;
    Node* theNext = first-> next;
    if( theNext != nullptr ) {	// if queue is nonempty
      T* val = theNext->value;	// take it out
      theNext->value = nullptr;	// of the Node
      first = theNext;		// swing first forward
      consumerLock = false;	// release exclusivity
      result = *val;		// now copy it back
      delete val;  		// clean up the value
      delete theFirst;		// and the old dummy
      return true;		// and report success
    }

    consumerLock = false;	// release exclusivity
    return false;		// report queue was empty
  }
};

#endif // LOCKFREEQUEUE_H

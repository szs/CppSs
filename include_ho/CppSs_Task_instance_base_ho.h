
#ifndef TASK_INSTANCE_BASE_H
#define TASK_INSTANCE_BASE_H

#include "CppSs_includes.h"
#include "CppSs_Task_functor_base_ho.h"

class CppSs_runtime;

class Task_instance_base
{
    public:
        explicit Task_instance_base(CppSs_runtime *rt);
        virtual ~Task_instance_base();
        void add_successor(int64_t task_id);
//        Task_instance_base(Task_functor_base *f) : m_f(f){}
        const int64_t get_id() const {return m_id;}
        virtual void run(intptr_t thrd_id) const =0;
//        virtual std::string get_func_name() const =0;
        const int64_t get_predecessor_count() const;
        const bool has_successors() const;
        const std::vector<int64_t> &get_successors() const;
        const int64_t increase_predecessor_count();
        const int64_t decrease_predecessor_count();
        const int64_t get_priority() const {return m_priority;}
        void set_priority(const int64_t b){m_priority = b;}
        void enqueue_free_successors();
        //void set_thrd_id(int id);
    protected:
        const int64_t m_id;
        const CppSs_runtime *m_runtime;

//        std::mutex m_successor_list_mutex;
//        std::mutex m_predecessor_count_mutex;

        int64_t m_priority;
        volatile int64_t m_predecessor_count;
        std::vector<int64_t> m_successors;

        //int m_thrd_id;
};



//#include "CppSs_Task_instance_base.h"


Task_instance_base::Task_instance_base(CppSs_runtime *rt)
    : /*m_thrd_id(-1),*/ m_id(returnUniqueTaskId()), m_runtime(rt), m_priority(0), m_predecessor_count(0)
{
  //    m_successors.reserve(1000);
}

Task_instance_base::~Task_instance_base()
{
//    LOG(DEBUG) << "~Task_instance_base " << m_id;
}

void Task_instance_base::add_successor(int64_t task_id)
{
//    std::lock_guard<std::mutex> local_mutex(m_successor_list_mutex);
    m_successors.push_back(task_id);
}

const int64_t Task_instance_base::get_predecessor_count() const
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
#if CPPSS_DEBUG
    if(m_predecessor_count<0){
        LOG(ERROR) << "m_predecessor_count < 0";
    }
#endif
    return m_predecessor_count;
}

const bool Task_instance_base::has_successors() const
{
//    std::lock_guard<std::mutex> local_mutex(m_successor_list_mutex);
    return !m_successors.empty();
}

const std::vector<int64_t> &Task_instance_base::get_successors() const
{
    return m_successors;
}

const int64_t Task_instance_base::increase_predecessor_count()
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
    return __sync_add_and_fetch(&m_predecessor_count,1);
}

const int64_t Task_instance_base::decrease_predecessor_count()
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
    return __sync_sub_and_fetch(&m_predecessor_count,1);
}
#endif // TASK_INSTANCE_BASE_H

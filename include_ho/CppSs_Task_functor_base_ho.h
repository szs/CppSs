/*
 * CPPSs_Task_functor_base.hh
 *
 * header file for the CPPSs Task_functor base class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


#ifndef CPPSS_TASK_FUNCTOR_BASE_H
#define CPPSS_TASK_FUNCTOR_BASE_H

#include "CppSs_defines.h"
#include "CppSs_helper_ho.h"
#include "CppSs_includes.h"
#include "CppSs_runtime_ho.h"

class CppSs_runtime;

class Task_functor_base
{
    public:
  explicit Task_functor_base (std::initializer_list<dep_t> deps, std::string func_name, CppSs_runtime *rt, int64_t prio=0);
        virtual ~Task_functor_base(){}
        const std::string get_func_name() const {return m_func_name;};
        const int64_t get_func_id() const {return m_func_id;};
        const CppSs_runtime *get_runtime () const {return m_runtime;}
        const int64_t get_priority() const {return m_priority;}
        void set_priority(int64_t b){m_priority = b;}


    protected:
        const int64_t m_func_id;
        std::vector<dep_t> m_deps;
        std::vector<std::type_info const*> m_types;
        const std::string m_func_name;
        CppSs_runtime *m_runtime;
	int64_t m_priority;
};

/*
 * CPPSs_Task_functor_base.cc
 *
 * source file for the CPPSs_Task_functor_base class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

//#include "CppSs_Task_functor_base.h"

Task_functor_base::Task_functor_base(std::initializer_list<dep_t> deps, std::string func_name, CppSs_runtime *rt, int64_t prio)
  : m_func_id(returnUniqueFuncId()), m_func_name(func_name), m_runtime(rt), m_priority(prio)
{
  //    m_types.reserve(8);
  //    m_deps.reserve(8);
    for (auto d : deps) m_deps.push_back(d);

#if USE_AYUDAME
    LOG(DEBUG1) << "AYU_REGISTER_FUNCTION, " << func_name.c_str();
    if (AYU_event) {
      AYU_event(AYU_REGISTERFUNCTION, m_func_id, (void *)func_name.c_str());
    }
#endif

#if CPPSS_PROFILING
    CppSs_Profiling::init_counter(m_func_id, m_func_name);
#endif

}

//std::string Task_functor_base::get_func_name() const
//{
//    return m_func_name;
//}
//
//int64_t Task_functor_base::get_func_id() const
//{
//    return m_func_id;
//}
#endif // CPPSS_TASK_FUNCTOR_BASE_H

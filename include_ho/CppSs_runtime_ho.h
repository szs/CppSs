/*
 * CPPSs_runtime.hh
 *
 * header file for the CPPSs_runtime class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


#ifndef CPPSS_RUNTIME_H
#define CPPSS_RUNTIME_H

//#include "CppSs_helper.h"
#include "CppSs_profiling_ho.h"
#include "CppSs_includes.h"
#include "CppSs_LowLockQueue.h"
#include "CppSs_Task_instance_base_ho.h"

class Global_queue;

class CppSs_runtime
{
    public:
        CppSs_runtime();
        ~CppSs_runtime();

        void add_worker(int64_t n_worker);
        const int get_n_worker() const {return m_worker.size();};
        const int get_n_threads() const {return m_worker.size()+1;};
        void stop_all_threads();
        const bool should_stop_thread(intptr_t i) const;

        const int64_t new_task(std::shared_ptr<Task_instance_base> task_p);
        void run_task(int64_t task_id, CppSs_runtime *runtime, int64_t i);
        const std::shared_ptr<Task_instance_base> get_task(int64_t task_id) const;
        void enqueue_free_successors(int64_t task_id);
        const int64_t get_priority(int64_t task_id) const;

        const bool add_to_last_writers(void *v, int64_t task_id);
        void remove_task_from_last_writers(int64_t task_id);

        void add_to_last_readers(void *v, int64_t task_id);
        void remove_task_from_last_readers(int64_t task_id);

        void add_to_last_reductions(void *v, int64_t task_id);
        void remove_task_from_last_reductions(int64_t task_id);

        const bool check_dep_on_last_reductions(int64_t task_id, void *v);
        const bool check_dep_on_last_writers(int64_t task_id, void *v) const;
        const bool check_dep_on_last_readers(int64_t task_id, void *v);

        void queue_task(const int64_t task_id);
        bool dequeue_task( int64_t& task_id );

        const int64_t get_n_tasks() const {return m_tasks.size();};
        //const int64_t get_n_global_queue() const {return m_global_queue.size();};
        const int64_t get_n_unfinished_tasks() const {return m_unfinished_tasks_counter;};
        const int64_t get_n_total_tasks() const {return m_total_tasks_counter;};

#if CPPSS_DEBUG
        void print_all_tasks() const;
#endif

    private:
        static void dispatch_task(intptr_t i, CppSs_runtime *rt);
        void increase_unfinished_tasks_counter();
        void decrease_unfinished_tasks_counter();
        void increase_total_tasks_counter();
        void decrease_total_tasks_counter();

        volatile int64_t m_unfinished_tasks_counter;
        volatile int64_t m_total_tasks_counter;

        std::map< void*, int64_t > m_last_writers;
        std::multimap< void*, int64_t > m_last_readers;
        std::multimap< void*, int64_t > m_last_reductions;
        LowLockQueue<int64_t> m_global_queue;
        LowLockQueue<int64_t> m_priority_queue;
        std::vector<std::shared_ptr<Task_instance_base>> m_tasks;

        std::vector<bool> m_stop_thread_vec;
        std::vector<std::shared_ptr<std::thread>> m_worker;
};

//inline void CppSs_runtime::increase_task_counter()
//{
//    __sync_add_and_fetch(&m_task_counter,1);
//}
//
//inline void CppSs_runtime::decrease_task_counter()
//{
//    __sync_sub_and_fetch(&m_task_counter,1);
//}


/*
 * CPPSs_runtime.hh
 *
 * source file for the CPPSs_runtime class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


//#include "CppSs_runtime.h"
//#include "CppSs_Task_instance_base.h"

std::mutex CppSs_last_writers_mutex;
//std::mutex CppSs_last_readers_mutex;
//std::mutex CppSs_last_reductions_mutex;

//std::mutex CppSs_finished_tasks_mutex;
//std::mutex CppSs_tasks_counter_mutex;
std::mutex CppSs_stop_thread_vec_mutex;
std::mutex CppSs_all_tasks_mutex;

CppSs_runtime::CppSs_runtime()
  : m_unfinished_tasks_counter(0),
    m_total_tasks_counter(0)
{
  //    m_stop_thread_vec.reserve(100);
  //    m_worker.reserve(100);
}

CppSs_runtime::~CppSs_runtime()
{
    LOG(DEBUG2) << "~CppSs_runtime";
}

void CppSs_runtime::dispatch_task(int64_t i, CppSs_runtime *runtime)
{

  switch ( i ){
  case 1:
    CppSs_Profiling::start_clock(3, "tot_t_thr1", CLOCK_THREAD_CPUTIME_ID);
    break;
  case 2:
    CppSs_Profiling::start_clock(4, "tot_t_thr2", CLOCK_THREAD_CPUTIME_ID);
    break;
  case 3:
    CppSs_Profiling::start_clock(5, "tot_t_thr3", CLOCK_THREAD_CPUTIME_ID);
    break;
  }

    //LOG(DEBUG1) << "+++++++++++++ thread " << i << " started";
    while(!runtime->should_stop_thread(i)){
        int64_t task_id(0);
        if ( runtime->dequeue_task(task_id) ) {
            runtime->run_task(task_id, runtime, i);
        }
    }

    switch ( i ){
    case 1:
      CppSs_Profiling::stop_clock(3);
      break;
    case 2:
      CppSs_Profiling::stop_clock(4);
      break;
    case 3:
      CppSs_Profiling::stop_clock(5);
    break;
    }

}

const int64_t CppSs_runtime::new_task(std::shared_ptr<Task_instance_base> task_p)
{
    std::lock_guard<std::mutex> local_mutex(CppSs_all_tasks_mutex);

    m_tasks.push_back(task_p);
    increase_unfinished_tasks_counter();
    increase_total_tasks_counter();

#if  CPPSS_DEBUG
//    if(task_p->get_id() > 100000) {
//        LOG(ERROR) << __FILE__ << ":" << __LINE__ << " taskid too large.";
//    }
    if(get_n_total_tasks()-1 != task_p->get_id()) {
        LOG(ERROR) << __FILE__ << ":" << __LINE__ << " get_n_total_tasks()-1 != m_tasks.back()->get_id()";
    }
#endif

    return task_p->get_id();
}

void CppSs_runtime::run_task(int64_t task_id,CppSs_runtime *runtime, int64_t i)
{
#if CPPSS_TRACING
    LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 0 " << task_id;
#endif

    // get a task pointer
    std::shared_ptr<Task_instance_base> task_p(runtime->get_task(task_id));

#if CPPSS_DEBUG
//    LOG(WARNING) << task_id;
    if ( task_p.use_count() != 2) {
        LOG(ERROR) << __FILE__ << ":" << __LINE__ << " task " << task_id << " taken. " << task_p.use_count();
    }
#endif

    // run task
    task_p->run(i);
    {
        std::lock_guard<std::mutex> local_mutex(CppSs_last_writers_mutex);

#if CPPSS_TRACING
        LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 1 " << task_id;
#endif
        // remove task from last_writers, last_readers and last_reductions
        runtime->remove_task_from_last_writers(task_id);
#if CPPSS_TRACING
        LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 2 " << task_id;
#endif
        runtime->remove_task_from_last_readers(task_id);
#if CPPSS_TRACING
        LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 3 " << task_id;
#endif
        runtime->remove_task_from_last_reductions(task_id);
#if CPPSS_TRACING
        LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 4 " << task_id;
#endif
    }


    // find free successors and enqueue them
    runtime->enqueue_free_successors(task_id);

    // decrease global counter of unfinished tasks
    runtime->decrease_unfinished_tasks_counter();
#if CPPSS_TRACING
    LOG(TRACE) << i << " 4 run_task " << rdtsc() << " 9 " << task_id;
#endif
}

const std::shared_ptr<Task_instance_base> CppSs_runtime::get_task(int64_t task_id) const
{
    std::lock_guard<std::mutex> local_mutex(CppSs_all_tasks_mutex);
    //    LOG(INFO) << task_id << " " << task_id << " " << m_tasks.capacity() << " " << m_tasks.max_size();
    try {
        const std::shared_ptr<Task_instance_base> sp(m_tasks.at(task_id));
        return sp;
    }
    catch(std::exception& e){
        LOG(ERROR) << "accessing m_tasks: " << e.what();
        LOG(ERROR) << task_id << " " << task_id << " " << m_tasks.capacity();
        return nullptr;
    }
}

void CppSs_runtime::enqueue_free_successors(int64_t task_id)
{
    std::lock_guard<std::mutex> local_mutex(CppSs_all_tasks_mutex);
    auto successor_list = m_tasks[task_id]->get_successors();
    for (auto suc_id : successor_list){
//        LOG(DEBUG) << "suc " << suc->get_id() << " " << suc->get_predecessor_count();
        if (m_tasks[suc_id]->decrease_predecessor_count() == 0){
#if USE_AYUDAME
	  //            LOG(DEBUG1) << "enqueue suc " << suc_id << " from task " << task_id;
            int64_t th_id=0;
            if (AYU_event) { AYU_event(AYU_ADDTASKTOQUEUE, suc_id, &th_id); }
#endif
            queue_task(suc_id);
        }
    }
}

void CppSs_runtime::add_worker(int64_t n_threads)
{
  for (int64_t i = 1; i < n_threads; ++i){
    LOG(INFO) << "adding worker: " << i << " of " << n_threads;
        {
            std::lock_guard<std::mutex> local_mutex(CppSs_stop_thread_vec_mutex);
            m_stop_thread_vec.push_back(false);
        }
        try{
            std::shared_ptr<std::thread> thrd(new std::thread(CppSs_runtime::dispatch_task,i,this));
            m_worker.push_back(thrd);
        }
        catch (std::exception& e){
            LOG(ERROR) << "new thread caused exception: " << e.what();
        }
    }
}

void CppSs_runtime::stop_all_threads()
{
    {
        std::lock_guard<std::mutex> local_mutex(CppSs_stop_thread_vec_mutex);
        std::fill(m_stop_thread_vec.begin(),m_stop_thread_vec.end(),true);
    }
    for (auto thrd : m_worker){
        LOG(DEBUG1) << "stopping thread " << thrd->get_id();
        try{
            thrd->join();
        }
        catch(std::exception& e){
            LOG(ERROR) << "stopping thread caused exception: " << e.what();
        }
        LOG(DEBUG1) << "stopped thread " ;
        //delete thrd.get();
        //LOG(DEBUG1) << "deleted thread " ;
    }

}

const bool CppSs_runtime::add_to_last_writers(void *v, int64_t task_id)
{
    // already locked in CppSs_functor::operator()()
//    std::lock_guard<std::mutex> local_mutex(CppSs_last_writers_mutex);

    // try to add memory address to last_writers
    auto res = m_last_writers.insert(std::make_pair(v,task_id));

    // if memory address already existed, just set the new task_id
    if (res.second == false){
        res.first->second = task_id;
    }

    // false if memory address already existed and task overwrote former last writer
    // true if no other task writing to this memory address existed in last_writers
    return res.second;
}

void CppSs_runtime::add_to_last_readers(void *v, int64_t task_id)
{
    // already locked in CppSs_functor::operator()()
//    std::lock_guard<std::mutex> local_mutex(CppSs_last_readers_mutex);

    // save this task as last reader. Multiple tasks can be last readers of the same data
    // TODO: use emplace
    m_last_readers.insert(std::make_pair(v,task_id));
    return;
}

void CppSs_runtime::add_to_last_reductions(void *v, int64_t task_id)
{
    // already locked in CppSs_functor::operator()()

    // save this task as last reduction. Multiple tasks can be last reductions of the same data
    // TODO: use emplace
    m_last_reductions.insert(std::make_pair(v,task_id));

    return;
}

void CppSs_runtime::remove_task_from_last_writers(int64_t task_id)
{
  // already locked in CppSs_runtime::run_task

    for( auto it = m_last_writers.begin();
        (it = std::find_if(it,m_last_writers.end(),
                           [task_id]
                           (std::pair<void*,int64_t> p)
                           {return p.second == task_id;})) != m_last_writers.end();){
        m_last_writers.erase(it++);
    }
}

void CppSs_runtime::remove_task_from_last_readers(int64_t task_id)
{
    // already locked in CppSs_runtime::run_task

    for( auto it = m_last_readers.begin();
        (it = std::find_if(it,m_last_readers.end(),
                           [task_id]
                           (std::pair<void*,int64_t> p)
                           {return p.second == task_id;})) != m_last_readers.end();){
        m_last_readers.erase(it++);
    }
}

void CppSs_runtime::remove_task_from_last_reductions(int64_t task_id)
{
    // already locked in CppSs_runtime::run_task

    for( auto it = m_last_reductions.begin();
        (it = std::find_if(it,m_last_reductions.end(),
                           [task_id]
                           (std::pair<void*,int64_t> p)
                           {return p.second == task_id;})) != m_last_reductions.end();){
        m_last_reductions.erase(it++);
    }
}

const bool CppSs_runtime::should_stop_thread(intptr_t i) const
{
    //~ std::lock_guard<std::mutex> local_mutex(CppSs_stop_thread_vec_mutex);
    return m_stop_thread_vec[i-1];
}

// TODO: use atomic variables instead of gcc builtins
void CppSs_runtime::increase_total_tasks_counter()
{
    __sync_add_and_fetch(&m_total_tasks_counter,1);
}

void CppSs_runtime::increase_unfinished_tasks_counter()
{
    __sync_add_and_fetch(&m_unfinished_tasks_counter,1);
}

void CppSs_runtime::decrease_unfinished_tasks_counter()
{
    __sync_sub_and_fetch(&m_unfinished_tasks_counter,1);
}

void CppSs_runtime::queue_task(int64_t task_id)
{
    if ( m_tasks[task_id] != nullptr ){
        if ( m_tasks[task_id]->get_priority() == 0 ){
            m_global_queue.add_task(task_id);
        }
        else{
            m_priority_queue.add_task(task_id);
        }
    }
    else{
        LOG(ERROR) << "Tried to queue task " << task_id << " wich is null!";
    }
}

bool CppSs_runtime::dequeue_task(int64_t& task_id)
{
    if ( m_priority_queue.get_task(task_id) ){
        return true;
    }
    return m_global_queue.get_task(task_id);
}

const int64_t CppSs_runtime::get_priority(int64_t task_id) const
{
//    std::lock_guard<std::mutex> local_mutex(CppSs_all_tasks_mutex);
    return m_tasks[task_id]->get_priority();
}

const bool CppSs_runtime::check_dep_on_last_writers(int64_t task_id, void *v) const
{
    bool is_dependant = false;
//    std::lock_guard<std::mutex> local_mutex_all_tasks(CppSs_all_tasks_mutex);
//    std::lock_guard<std::mutex> local_mutex_last_writers(CppSs_last_writers_mutex);
    // find the last task to write to this memory address
    auto it_lw = m_last_writers.find(v);
    if (it_lw != m_last_writers.end()) {
        // add task as successor and replace last writer by task
        m_tasks[it_lw->second]->add_successor(task_id);
        m_tasks[task_id]->increase_predecessor_count();
        is_dependant = true;
#if USE_AYUDAME
//        LOG(DEBUG1) << "dep " << it_lw->second << " " << task_id;
        {
        uintptr_t Ayu_data[3]={0,0,0};
        Ayu_data[0] = it_lw->second;
        Ayu_data[1] = (uintptr_t) v;
        Ayu_data[2] = (uintptr_t) v;
        if (AYU_event) { AYU_event(AYU_ADDDEPENDENCY, task_id, (void*) Ayu_data); }
        }
#endif
    }
    return is_dependant;
}

const bool CppSs_runtime::check_dep_on_last_readers(int64_t task_id, void *v)
{
    bool is_dependant = false;
    // find last reader(s)
    // std::multimap< void*, int64_t >::iterator it_lr;
//    std::lock_guard<std::mutex> local_mutex_all_tasks(CppSs_all_tasks_mutex);
//    std::lock_guard<std::mutex> local_mutex_last_readers(CppSs_last_readers_mutex);

    auto it_lr = m_last_readers.begin();
    while((it_lr = std::find_if(it_lr, m_last_readers.end(), [v](std::pair<void*,int64_t> p){return p.first == v;})) != m_last_readers.end() ) {
        m_tasks[it_lr->second]->add_successor(task_id);
        m_tasks[task_id]->increase_predecessor_count();
        // mark as dependant
        is_dependant = true;
#if USE_AYUDAME
//        LOG(DEBUG1) << "dep " << it_lr->second << " " << task_id;
        {
        uintptr_t Ayu_data[3]={0,0,0};
        Ayu_data[0] = it_lr->second;
        Ayu_data[1] = (uintptr_t) v;
        Ayu_data[2] = (uintptr_t) v;
        if (AYU_event) { AYU_event(AYU_ADDDEPENDENCY, task_id, (void*) Ayu_data); }
        }
#endif
        m_last_readers.erase(it_lr++);
    }
    return is_dependant;
}

const bool CppSs_runtime::check_dep_on_last_reductions(int64_t task_id, void *v)
{
    bool is_dependant = false;
//    std::lock_guard<std::mutex> local_mutex_all_tasks(CppSs_all_tasks_mutex);
//    std::lock_guard<std::mutex> local_mutex_last_reductions(CppSs_last_reductions_mutex);
    auto it_red = m_last_reductions.begin();
    while((it_red = std::find_if(it_red, m_last_reductions.end(),
				 [v](std::pair<void*,int64_t> p){
				   return p.first == v;
				 })) != m_last_reductions.end() ) {
        m_tasks[it_red->second]->add_successor(task_id);
        m_tasks[task_id]->increase_predecessor_count();
        // mark as dependant
        is_dependant = true;
#if USE_AYUDAME
//        LOG(DEBUG1) << "dep " << it_red->second << " " << task_id;
        {
        uintptr_t Ayu_data[3]={0,0,0};
        Ayu_data[0] = it_red->second;
        Ayu_data[1] = (uintptr_t) v;
        Ayu_data[2] = (uintptr_t) v;
        if (AYU_event) { AYU_event(AYU_ADDDEPENDENCY, task_id, (void*) Ayu_data); }
        }
#endif
        m_last_reductions.erase(it_red++);
    }

    return is_dependant;
}

#if CPPSS_DEBUG
void CppSs_runtime::print_all_tasks() const
{
    std::lock_guard<std::mutex> local_mutex(CppSs_all_tasks_mutex);
    for (auto task_p : m_tasks){
        LOG(DEBUG4) <<  task_p->get_id() << " " << task_p->get_func_name();
    }
}
#endif
#endif // CPPSS_RUNTIME_H

/*
 * CPPSs_Task_instance.h
 *
 * header file for the CPPSs Task_instance class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#include "CppSs_includes.h"
#include "CppSs_Task_functor_base_ho.h"
#include "CppSs_Task_instance_base_ho.h"


template<typename... ARGS>
class Task_instance : public Task_instance_base
{
    public:
        explicit Task_instance(Task_functor_base *f, CppSs_runtime* runtime);
        ~Task_instance();
        void run(const int64_t thrd_id) const;
        void set_argument_pack(std::tuple<ARGS...> *ap);
//        std::string get_func_name() const;
    private:
        Task_functor_base *m_functor;
        std::tuple<ARGS...> *m_argument_pack;
};


template<typename... ARGS>
Task_instance<ARGS...>::Task_instance(Task_functor_base *f, CppSs_runtime* runtime)
    : Task_instance_base(runtime), m_functor(f), m_argument_pack(nullptr)
{
//    LOG(DEBUG3) << "created task_instance " << m_id;
  m_priority = m_functor->get_priority();
}

template<typename... ARGS>
Task_instance<ARGS...>::~Task_instance()
{
//    LOG(DEBUG3) << "~Task_instance - " << m_id ;
    if (m_argument_pack != nullptr){
        delete m_argument_pack;
    }
}

template<typename... ARGS>
void Task_instance<ARGS...>::set_argument_pack(std::tuple<ARGS...> *ap)
{
    m_argument_pack = ap;
}

template<typename... ARGS>
void Task_instance<ARGS...>::run(int64_t thrd_id) const
{
#if CPPSS_TRACING
    LOG(TRACE) << thrd_id << " 3 " << m_functor->get_func_name() << " " << rdtsc() << " 0 " << m_id ;
#endif

#if USE_AYUDAME
    if (AYU_event) { AYU_event(AYU_PRERUNTASK, m_id, &thrd_id); }
#endif

#if CPPSS_PROFILING
    CppSs_Profiling::increase_counter(m_functor->get_func_id());
#endif

//    LOG(DEBUG1) << "running " << m_functor->get_func_name() << " id " << get_id();
    applyTuple(((Task_functor<ARGS...>*)m_functor)->m_f, *m_argument_pack);
//    LOG(DEBUG1) << "ran " << m_functor->get_func_name() << " id " << get_id();


#if USE_AYUDAME
    if (AYU_event) { AYU_event(AYU_REMOVETASK, m_id, nullptr); }
#endif
#if CPPSS_TRACING
    LOG(TRACE) << thrd_id << " 3 " << m_functor->get_func_name() << " " << rdtsc() << " 9 " << m_id;
#endif

}

//template<typename... ARGS>
//std::string Task_instance<ARGS...>::get_func_name() const
//{
//    return m_functor->get_func_name();
//}

/*
 * CPPSs_includes.hh
 *
 * header file for the CPPSs internal include list
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#include "CppSs_defines.h"

#if USE_AYUDAME
#include <Ayudame.h>
#endif

#include "CppSs_logging.h"
#if CPPSS_PROFILING == 1
    #include <ctime>
    #include "CppSs_profiling_ho.h"
#endif

// These semm not to be necessary, even though e.g. int64_t is used.
//#include <cstdint.h>
//#include <cstdlib>

#include <algorithm>
#include <atomic>
#include <deque>
#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <typeinfo>
#include <vector>


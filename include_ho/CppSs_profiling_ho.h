#ifndef CPPSS_PROFILING_H
#define CPPSS_PROFILING_H

/*
 * CppSs_profiling.h
 *
 * header file CppSs_profiling.h for profiling capabilities
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#include "CppSs_includes.h"
#include <atomic>
#include <memory.h>

namespace CppSs_Profiling{

  /* vectors for clocks */
  extern std::vector<timespec> time_vec;
  extern std::vector<timespec> timetmp_vec;
  extern std::vector<std::string> clockname_vec;
  extern std::vector<clockid_t> cid_vec;
  extern std::vector<bool> running_vec;

  /* clock functions */
  void start_clock(unsigned id, std::string s, clockid_t cid=CLOCK_REALTIME);
  void restart_clock(unsigned id);
  void stop_clock(unsigned id);
  void reset_clock(unsigned id);

  /* vectors for counters */
  extern std::vector<unsigned> counter_vec;
  extern std::vector<std::string> countername_vec;

  /* counter functions */
  void init_counter(unsigned id, std::string s);
  void increase_counter(unsigned id);
  void decrease_counter(unsigned id);
  void reset_counter(unsigned id);

  /* report */
  std::string report();



}

/*
 * CppSs_profiling.cc
 *
 * implementation file CppSs_profiling.cc for profiling capabilities
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

//#include "CppSs_profiling.h"

namespace CppSs_Profiling{

  /* ------------------------------------------------------------------- */
  /* vector definitions */

  std::vector<timespec> time_vec(10);
  std::vector<timespec> timetmp_vec(10);
  std::vector<std::string> clockname_vec(10);
  std::vector<clockid_t> cid_vec(10);
  std::vector<bool> running_vec(10,false);

  std::vector<unsigned> counter_vec(10);
  std::vector<std::string> countername_vec(10);

  /* ------------------------------------------------------------------- */
  /* helper funcitons */

  timespec diff(timespec start, timespec end)
  {
    timespec temp;
    if ( ( end.tv_nsec - start.tv_nsec ) < 0 ) {
      temp.tv_sec = end.tv_sec-start.tv_sec - 1;
      temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
      temp.tv_sec = end.tv_sec - start.tv_sec;
      temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
  }

  timespec add(timespec a, timespec b)
  {
    timespec temp;
    if ( ( a.tv_nsec + b.tv_nsec ) > 1000000000 ) {
      temp.tv_sec = b.tv_sec + a.tv_sec + 1;
      temp.tv_nsec = a.tv_nsec + b.tv_nsec - 1000000000;
    } else {
      temp.tv_sec = a.tv_sec + b.tv_sec;
      temp.tv_nsec = a.tv_nsec + b.tv_nsec;
    }
    return temp;
  }

  /* ------------------------------------------------------------------- */
  /* clock functions */

  void start_clock(unsigned id, std::string s, clockid_t cid)
  {
    /* resize vectors if necessary */
    if ( id >= time_vec.size() ){
      time_vec.resize(2*id);
      timetmp_vec.resize(2*id);
      clockname_vec.resize(2*id);
      cid_vec.resize(2*id);
      running_vec.resize(2*id);
    }

    /* catch some unwanted behaviour */
    if ( s == "" ) {
      LOG(ERROR) << "clock " << id << " started without a name. Please provide a name";
      return;
    }
    if ( clockname_vec[id] != "" ){
      LOG(ERROR) << "clock with id " << id
		   << " and name " << clockname_vec[id] << " already exists. ";
      return;
    }
    if ( running_vec[id] == true ){
      LOG(ERROR) << "clock with id " << id
		 << " and name " << clockname_vec[id] << " already running. ";
      return;
    }

    /* measure time */
    timespec temp;
    if (clock_gettime(cid, &temp) != 0){
      LOG(ERROR) << "clock_gettime in start_clock( " << (int) id
		 << " , " << s << " , " << cid << " ) failed.";
    }
    else{
      //double t = (double) temp.tv_sec + (double) temp.tv_nsec / 1.e-9;
      LOG(INFO) << "start_clock( " << id << " ) at " << temp.tv_sec << " " << temp.tv_nsec;
      timetmp_vec[id] = temp;
      clockname_vec[id] = s;
      cid_vec[id] = cid;
      running_vec[id] = true;
    }
  }

  void restart_clock(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= time_vec.size()){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot restart.";
      return;
    }
    if ( clockname_vec[id] == "" ){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot restart.";
      return;
    }
    if ( running_vec[id] == true ){
      LOG(ERROR) << "clock with id " << id
		 << " and name " << clockname_vec[id] << " already running. ";
      return;
    }

    /* measure time */
    timespec temp;
    if (clock_gettime(cid_vec[id], &temp) != 0){
      LOG(ERROR) << "clock_gettime in restart_clock( " << (int) id
		 << " ) failed.";
    }
    else{
      //double t = (double) temp.tv_sec + (double) temp.tv_nsec / 1.e-9;
      LOG(INFO) << "restart_clock( " << id << " ) at " << temp.tv_sec << " " << temp.tv_nsec;
      timetmp_vec[id] = temp;
      running_vec[id] = true;
    }
  }

  void stop_clock(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= time_vec.size()){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot stop.1";
      return;
    }
    if ( clockname_vec[id] == "" ){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot stop.2";
      return;
    }
    if ( running_vec[id] == false ){
      LOG(ERROR) << "clock with id " << id << " and name " << clockname_vec[id]
		 << " is not running. Cannot stop";
      return;
    }

    /* measure time */
    timespec temp;
    if (clock_gettime(cid_vec[id], &temp) != 0){
      LOG(ERROR) << "clock_gettime in stop_clock( " << (int) id
		 << " ) failed.";
    }
    else{
      double t = (double) temp.tv_sec + (double) temp.tv_nsec / 1.e-9;
      LOG(INFO) << "stop_clock( " << id << " ) at " << temp.tv_sec << " " << temp.tv_nsec;
      time_vec[id] = add(time_vec[id],diff(timetmp_vec[id],temp));
      running_vec[id] = false;
    }
  }

  void reset_clock(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= time_vec.size()){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot reset.";
      return;
    }
    if ( clockname_vec[id] != "" ){
      LOG(ERROR) << "clock with id " << id << " does not exist. Cannot reset.";
      return;
    }
    if ( running_vec[id] == true ){
      LOG(ERROR) << "clock with id " << id << " and name " << clockname_vec[id]
		 << " is running. Cannot reset";
      return;
    }

    /* reset data */
    time_vec[id] = {0,0};
    timetmp_vec[id] = {0,0};
    clockname_vec[id] = "";
    cid_vec[id] = 0;
  }

  /* ------------------------------------------------------------------- */
  /* clock functions */

  void init_counter(unsigned id, std::string s)
  {
    /* resize vectors if necessary */
    if (id >= counter_vec.size() ) {
      counter_vec.resize(2*id);
      countername_vec.resize(2*id);
    }

    /* catch some unwanted behaviour */
    if ( s == "" ) {
      LOG(ERROR) << "counter " << id << " initialised without a name. Please provide a name";
      return;
    }
    if ( countername_vec[id] != "" ){
      LOG(WARNING) << "counter with id " << id
		   << " and name " << clockname_vec[id] << " already exists. ";
      return;
   }

    /* set counter name */
    countername_vec[id] = s;

  }

  void increase_counter(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= counter_vec.size()){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.1";
      return;
    }
    if ( countername_vec[id] == "" ){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.2";
      return;
    }

    /* increase counter */
    ++counter_vec[id];
  }

  void decrease_counter(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= counter_vec.size()){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.";
      return;
    }
    if ( countername_vec[id] == "" ){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.";
      return;
    }

    /* decrease counter */
    --counter_vec[id];
  }

  void reset_counter(unsigned id)
  {
    /* catch some unwanted behaviour */
    if ( id >= counter_vec.size()){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.";
      return;
    }
    if ( countername_vec[id] == "" ){
      LOG(ERROR) << "counter with id " << id << " does not exist. Cannot increase.";
      return;
    }

    /* resetcounter */
    counter_vec[id]=0;
  }

  /* ------------------------------------------------------------------- */
  /* report function */

  std::string report()
  {
    std::string s;

    /* clocks */
    s += "\n### clocks ###\n#name \tcid \ttime\n";
    char buffer[100];
    for (unsigned i=0; i < time_vec.size(); ++i){
      if ( clockname_vec[i] == "" ) {continue;}
      memset(buffer,0,100);
      sprintf(buffer,"%s\t%d\t%lu.%lu\n",clockname_vec[i].c_str(), cid_vec[i], time_vec[i].tv_sec, time_vec[i].tv_nsec);
      s += buffer;
    }

    /* counters */
    s += "\n### counters ###\n#name \tvalue\n";
    for (unsigned i=0; i < counter_vec.size(); ++i){
      if ( countername_vec[i] == "" ) {continue;}
      memset(buffer,0,100);
      sprintf(buffer,"%s\t%u\n",countername_vec[i].c_str(), counter_vec[i]);
      s += buffer;
    }

    return s;
  }


}

#endif // CPPSS_PROFILING_H

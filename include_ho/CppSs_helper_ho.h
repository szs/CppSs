/*
 * CPPSs_Task_functor.h
 *
 * header file for the CPPSs helpers
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


#ifndef TASK_HELPER_H
#define TASK_HELPER_H

//#include <Ayudame.h>

#include "CppSs_includes.h"
//#include "CppSs_Task_instance.h"
//
template<typename... ARGS> class Task_functor;
class Task_instance_base;

extern unsigned int returnUniqueFuncId(int set_id = -1);
extern unsigned int returnUniqueTaskId(int set_id = -1);


typedef enum dependencies{
    IN,
    OUT,
    INOUT,
    REDUCTION,
    PARAMETER,
    MUTEX
} dep_t;


///////////////////////////
// TODO: type information needed?
// tuple: memory address, type (needed?), Task_instance pointer

//typedef std::tuple<void*, std::type_info const*, Task_instance_base*> dep_tuple;

///////////////////////////

template<typename T>
struct function_traits;

template<typename R, typename... ARGS>
struct function_traits<std::function<R(ARGS...)>>
{
    static const size_t nargs = sizeof...(ARGS);

    typedef R result_type;

    template <size_t i>
    struct arg
    {
        typedef typename std::tuple_element<i, std::tuple<ARGS...>>::type type;
    };
};


///////////////////////////////////////////
//
// TODO: types should better be passed as a pointer to make it clear
// in the calling code that it is going to be changed
//
template <typename fun, size_t i>
struct get_types_helper {
    static void get_types(std::vector<std::type_info const*> &types) {
        get_types_helper<fun, i-1>::get_types(types);
        types.push_back(&typeid(typename function_traits<fun>::template arg<i-1>::type));
    }
};

template <typename fun>
struct get_types_helper<fun,0> {
    static void get_types(std::vector<std::type_info const*> &types) {}
};

template <typename fun>
void get_types(std::vector<std::type_info const*> &types) {
    get_types_helper<fun, function_traits<fun>::nargs>::get_types(types);
}
//
//////////////////////////////////////////////

//////////////////////////////////////////////
//
template < uint N >
struct apply_func
{
  template < typename... ArgsF, typename... ArgsT, typename... Args >
  static void applyTuple( void (*f)( ArgsF... ),
                          const std::tuple<ArgsT...>& t,
                          Args... args )
  {
    apply_func<N-1>::applyTuple( f, t, std::get<N-1>( t ), args... );
  }
};

template <>
struct apply_func<0>
{
  template < typename... ArgsF, typename... ArgsT, typename... Args >
  static void applyTuple( void (*f)( ArgsF... ),
                          const std::tuple<ArgsT...>& /* t */,
                          Args... args )
  {
    f( args... );
  }
};

template < typename... ArgsF, typename... ArgsT >
void applyTuple( void (*f)(ArgsF...),
                 std::tuple<ArgsT...> const& t )
{
   apply_func<sizeof...(ArgsT)>::applyTuple( f, t );
}
//
//////////////////////////////////////////////

extern "C" {
	__inline__ uint64_t rdtsc(void) {
		uint32_t lo, hi;
		__asm__ __volatile__(	// serialize
					    "xorl %%eax,%%eax \n        cpuid":::"%rax",
					    "%rbx", "%rcx", "%rdx");
		__asm__ __volatile__("rdtsc":"=a"(lo), "=d"(hi));
		 return (uint64_t) hi << 32 | lo;
}}


/*
 * CPPSs_helper.cc
 *
 * source containing CPPSs helpers
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

//#include "CppSs_helper.h"

unsigned int returnUniqueFuncId(int set_id) {
    static volatile int unique_func_id = -1;
    if (set_id == -1){
      return __sync_add_and_fetch(&unique_func_id, 1);
    }
    return unique_func_id = set_id-1;
}

unsigned int returnUniqueTaskId(int set_id) {
    static volatile int unique_task_id = -1;
    if (set_id == -1){
      return __sync_add_and_fetch(&unique_task_id, 1);
    }
    return unique_task_id = set_id-1;
}

#endif // TASK_HELPER_H

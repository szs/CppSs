/*
 * CPPSs.h
 *
 * header file for the CPPSs runtime instance and user functions.
 * To be included in CppSs applications.
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#ifndef CPPSS_H
#define CPPSS_H

#include "CppSs_defines.h"
#include "CppSs_helper_ho.h"
#include "CppSs_profiling_ho.h"
#include "CppSs_logging.h"
#include "CppSs_runtime_ho.h"
#include "CppSs_Task_functor.h"

class Task_instance_base;

namespace CppSs{

#ifdef NO_CPPSS
    const static bool no_CppSs = true;
#else
    const static bool no_CppSs = false;
#endif


    extern CppSs_runtime *runtime;

    extern bool preInitDone;

    void PreInit();

    void Init(int n_threads = 2, TLogLevel ll = DEFAULT_REPORTINGLEVEL);

    void Finish();

    void Barrier();


    // wrap this factory in a singleton class?
#ifndef NO_CPPSS
    template<typename... ARGS>
      Task_functor<ARGS...> MakeTask(void(*f)(ARGS...),
				     std::initializer_list<dep_t> deps,
				     std::string func_name,
				     int64_t prio=0)
    {
        if (!preInitDone){
            PreInit();
            LOG(DEBUG) << "PreInit called by MakeTask().";
        }
        if (runtime == nullptr){
            LOG(ERROR) << "Runtime not present.";
        }

        return Task_functor<ARGS...>(f, deps, func_name, runtime, prio);
    }

#else // #ifndef NO_CPPSS

    template<typename... ARGS>
      std::function<void(ARGS...)> MakeTask(void(*f)(ARGS...),
					    std::initializer_list<dep_t> deps,
					    std::string func_name,
					    int64_t prio=0)
    {
        return f;
    }
#endif // #ifndef NO_CPPSS

}

/*
 * CPPSs.h
 *
 * source file for the CPPSs runtime instance and user functions.
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

//#include "CppSs.h"

namespace CppSs{
    CppSs_runtime *runtime = nullptr;
    bool preInitDone = false;

    void PreInit()
    {
      Log_t::ReportingLevel() = DEFAULT_REPORTINGLEVEL;

        if (!no_CppSs){
            LOG(DEBUG1) << "PreInit()";
            try{
                runtime = new CppSs_runtime;
                LOG(DEBUG1) << "sizeof(*runtime): " << sizeof(*runtime);
            }
            catch (std::exception& e){
                LOG(ERROR) << "new CppSs_runtime caused exception: " << e.what();
            }

#if USE_AYUDAME
        ayu_runtime_t ayu_rt = AYU_RT_CPPSS;
        if (AYU_event) { AYU_event(AYU_PREINIT, 0, (void*) &ayu_rt ); }
#endif
        }
        preInitDone = true;
    }

    void Init(int n_threads, TLogLevel ll)
    {
        Log_t::ReportingLevel() = ll;
#if CPPSS_TRACING
        LOG(TRACE) << "0 1 Init " << rdtsc() << " 0";
#endif
        LOG(INFO) << " ### CppSs::Init ###";

	CppSs_Profiling::start_clock(0, "tot_time");
	CppSs_Profiling::start_clock(1, "tot_t_thr0", CLOCK_THREAD_CPUTIME_ID);
	CppSs_Profiling::start_clock(2, "tot_t_pro", CLOCK_PROCESS_CPUTIME_ID);

        if (!preInitDone){
            PreInit();
        }
        Log_t::ReportingLevel() = ll;

        if (!no_CppSs){
#if USE_AYUDAME
        uint64_t n_thr(n_threads);
        if (AYU_event) { AYU_event(AYU_INIT, 0, &n_thr); }
#endif
            runtime->add_worker(n_threads);
            LOG(INFO) << "Running on " << runtime->get_n_worker()+1 << " threads.";
        }// if (!no_CppSs)

#if CPPSS_TRACING
        LOG(TRACE) << "0 1 Init " << rdtsc() << " 9";
#endif
    }

    void Finish()
    {
#if CPPSS_TRACING
        LOG(TRACE) << "0 99 Finish " << rdtsc() << " 0";
#endif

        if (!no_CppSs){
            if (!runtime) {
                LOG(WARNING) << "appearently called Finish() a second time. Runtime not present (anymore?)";
                return;
            }

    //        LOG(DEBUG1) << " CppSs::Finish ";
            Barrier();
    //        LOG(DEBUG1) << "stopping threads... ";
            runtime->stop_all_threads();
    //        LOG(DEBUG1) << "deleting runtime... ";
            LOG(INFO) << "Executed " << runtime->get_n_total_tasks() << " tasks.";

#if CPPSS_DEBUG
            runtime->print_all_tasks();
#endif

            LOG(DEBUG2) << " before delete runtime";
            delete runtime;
            LOG(DEBUG2) << " after delete runtime";
            runtime = nullptr;

	    preInitDone = false;
	    returnUniqueTaskId(0);
	    returnUniqueFuncId(0);

#if USE_AYUDAME
            if (AYU_event) { AYU_event(AYU_FINISH, 0, nullptr); }
#endif
            //LOG(INFO) << "### ran " << returnUniqueTaskId()-1 << " tasks. ###";
        } // if (!no_CppSs)

	CppSs_Profiling::stop_clock(0);
	CppSs_Profiling::stop_clock(1);
	CppSs_Profiling::stop_clock(2);

	LOG(INFO) << CppSs_Profiling::report();

        LOG(INFO) << " ### CppSs::Finish ###";
#if CPPSS_TRACING
        LOG(TRACE) << "0 99 Finish " << rdtsc() << " 9";
#endif
    }

    void Barrier()
    {
        if (!no_CppSs){

            LOG(DEBUG1) << "Barrier" ;
#if USE_AYUDAME
            if (AYU_event) { AYU_event(AYU_BARRIER, 0, nullptr); }
#endif
            LOG(DEBUG1) << "barrier waiting... taskcounter " << runtime->get_n_unfinished_tasks();
//                        << " queuesize " << runtime->get_n_global_queue();
            while (runtime->get_n_unfinished_tasks() > 0)
            {
    //        	std::this_thread::sleep_for(std::chrono::nanoseconds(100));
                // do this better with
                // std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
                // cond.wait(local_mutex,[]{return (runtime->get_task_counter() > 0);});
                // here and
                // std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
                // cond.notify_one();
                // in dispatch_task?
                // std::condition_variable cond; // somewhere...
                // get rid of
                // std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
                // in runtime->get_task_counter()!

                int64_t task_id(0); // this value is overwritten by dequeue_task
                if ( runtime->dequeue_task(task_id) ) {
                    LOG(DEBUG2) << "barrier runs task " << task_id;
                    runtime->run_task(task_id, runtime, 0);
                }
            }
            LOG(DEBUG2) << "Barrier done.";
        } // if (!no_CppSs)
    }
}
#endif // CPPSS_H

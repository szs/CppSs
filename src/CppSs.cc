/**
 * \file CppSs.cc
 *
 * \brief source file for the CPPSs runtime instance and user functions.
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2012-2014, Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 *
 */

#include "CppSs.h"

namespace CppSs{
    CppSs_runtime *runtime = nullptr;
    bool preInitDone = false;

    void PreInit()
    {
      Log_t::ReportingLevel() = DEFAULT_REPORTINGLEVEL;

        if (!no_CppSs){
            try{
                runtime = new CppSs_runtime;
                LOG(DEBUG2) << "sizeof(*runtime): " << sizeof(*runtime);
            }
            catch (std::exception& e){
                LOG(ERROR) << "new CppSs_runtime caused exception: " << e.what();
            }

#if USE_AYUDAME
        ayu_runtime_t ayu_rt = AYU_RT_CPPSS;
        if (AYU_event) { AYU_event(AYU_PREINIT, 0, (void*) &ayu_rt ); }
#endif
        }
        preInitDone = true;
    }

    void Init(int n_threads, TLogLevel ll)
    {
        Log_t::ReportingLevel() = ll;
#if CPPSS_TRACING
        LOG(TRACE) << "0 1 Init " << rdtsc() << " 0";
#endif
        LOG(INFO) << " ### CppSs::Init ###";
        LOG(DEBUG1) << " master thread id: " << std::this_thread::get_id();
#if CPPSS_PROFILING
        CppSs_Profiling::start_clock(0, "tot_time");
        CppSs_Profiling::start_clock(1, "tot_t_thr0", CLOCK_THREAD_CPUTIME_ID);
        CppSs_Profiling::start_clock(2, "tot_t_pro", CLOCK_PROCESS_CPUTIME_ID);
#endif // CPPSS_PROFILING

        if (!preInitDone){
            PreInit();
        }
        Log_t::ReportingLevel() = ll;

        if (!no_CppSs){
#if USE_AYUDAME
        uint64_t n_thr(n_threads);
        if (AYU_event) { AYU_event(AYU_INIT, 0, &n_thr); }
#endif
            runtime->add_worker(n_threads);
            LOG(INFO) << "Running on " << runtime->get_n_worker()+1 << " threads.";
        }// if (!no_CppSs)

#if CPPSS_TRACING
        LOG(TRACE) << "0 1 Init " << rdtsc() << " 9";
#endif
    }

    void Finish()
    {
#if CPPSS_TRACING
        LOG(TRACE) << "0 99 Finish " << rdtsc() << " 0";
#endif

        if (!no_CppSs){
            if (!runtime) {
                LOG(WARNING) << "appearently called Finish() a second time. Runtime not present (anymore?)";
                return;
            }

    //        LOG(DEBUG1) << " CppSs::Finish ";
            Barrier();
    //        LOG(DEBUG1) << "stopping threads... ";
            runtime->stop_all_threads();
    //        LOG(DEBUG1) << "deleting runtime... ";
            LOG(INFO) << "Executed " << runtime->get_n_total_tasks() << " tasks.";

#if CPPSS_DEBUG
            runtime->print_all_tasks();
#endif

            LOG(DEBUG2) << " before delete runtime";
            delete runtime;
            LOG(DEBUG2) << " after delete runtime";
            runtime = nullptr;

	    preInitDone = false;
	    returnUniqueTaskId(0);
	    returnUniqueFuncId(0);

#if USE_AYUDAME
            if (AYU_event) { AYU_event(AYU_FINISH, 0, nullptr); }
#endif
            //LOG(INFO) << "### ran " << returnUniqueTaskId()-1 << " tasks. ###";
        } // if (!no_CppSs)

#if CPPSS_PROFILING
	CppSs_Profiling::stop_clock(0);
	CppSs_Profiling::stop_clock(1);
	CppSs_Profiling::stop_clock(2);

	LOG(INFO) << CppSs_Profiling::report();
#endif // CPPSS_PROFILING

        LOG(INFO) << " ### CppSs::Finish ###";
#if CPPSS_TRACING
        LOG(TRACE) << "0 99 Finish " << rdtsc() << " 9";
#endif
    }

    void Barrier()
    {
        if (!no_CppSs){

            LOG(DEBUG1) << "Barrier of thread " << std::this_thread::get_id() ;
#if USE_AYUDAME
            if (AYU_event) { AYU_event(AYU_BARRIER, 0, nullptr); }
#endif
            LOG(DEBUG1) << "barrier waiting... taskcounter "
                        << runtime->get_n_unfinished_tasks(runtime->get_activity(std::this_thread::get_id()));
//                        << " queuesize " << runtime->get_n_global_queue();
            while (runtime->get_n_unfinished_tasks(runtime->get_activity(std::this_thread::get_id())) > 0) {
                //LOG(DEBUG1) << "in barrier: tasks for thread " << std::this_thread::get_id()
                //            << " : " << runtime->get_n_unfinished_tasks(runtime->get_activity(std::this_thread::get_id()));

            	//std::this_thread::sleep_for(std::chrono::nanoseconds(100));
                //std::this_thread::yield();
                int64_t task_id(CPPSS_NOTASK); // this value is overwritten by dequeue_task
                if ( runtime->dequeue_task(&task_id) ) {
                    LOG(DEBUG1) << "barrier runs task " << task_id;
                    LOG(DEBUG1) << "set_activity( " << std::this_thread::get_id() << ", " << task_id << " )";
                    /// \todo save stack of activities? push_activity and pop_activity
                    auto former_activity = runtime->set_activity(std::this_thread::get_id(), task_id);
                    runtime->run_task(task_id, runtime, 0);
                    runtime->set_activity(std::this_thread::get_id(), former_activity);
                }
            }
            LOG(DEBUG2) << "Barrier done.";
        } // if (!no_CppSs)
    }
}

/**
 * \file CPPSs_Task_functor_base.cc
 *
 * \brief source file for the CPPSs_Task_functor_base class
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2012-2014, Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 *
 */

#include "CppSs_Task_functor_base.h"

Task_functor_base::Task_functor_base(std::vector<dep_t> deps,
                                     std::string func_name,
                                     CppSs_runtime *rt,
                                     int64_t prio)
  : m_func_name(func_name),
    m_func_id(returnUniqueFuncId()),
    m_runtime(rt),
    m_priority(prio)
{
  //    m_types.reserve(8);
  //    m_deps.reserve(8);
    for (auto d : deps) m_deps.push_back(d);

#if USE_AYUDAME
    LOG(DEBUG1) << "AYU_REGISTER_FUNCTION, " << func_name.c_str();
    if (AYU_event) {
      AYU_event(AYU_REGISTERFUNCTION, m_func_id, (void *)func_name.c_str());
    }
#endif

#if CPPSS_PROFILING
    CppSs_Profiling::init_counter(m_func_id, m_func_name);
#endif

}

Task_functor_base::Task_functor_base(std::vector<dep_t> deps,
                                     std::string func_name,
                                     int64_t func_id,
                                     CppSs_runtime *rt,
                                     int64_t prio)
  : m_func_name(func_name),
    m_func_id(func_id),
    m_runtime(rt),
    m_priority(prio)
{
  //    m_types.reserve(8);
  //    m_deps.reserve(8);
    for (auto d : deps) m_deps.push_back(d);

#if USE_AYUDAME
    LOG(DEBUG1) << "AYU_REGISTER_FUNCTION, " << func_name.c_str();
    if (AYU_event) {
      AYU_event(AYU_REGISTERFUNCTION, m_func_id, (void *)func_name.c_str());
    }
#endif

#if CPPSS_PROFILING
    CppSs_Profiling::init_counter(m_func_id, m_func_name);
#endif

}

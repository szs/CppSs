/**
 * \file CPPSs_helper.cc
 *
 * \brief source containing CPPSs helpers
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2012-2014, Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 *
 */

#include "CppSs_helper.h"

unsigned int returnUniqueFuncId(int set_id) {
    static volatile int unique_func_id = -1;
    if (set_id == -1){
      return __sync_add_and_fetch(&unique_func_id, 1);
    }
    return unique_func_id = set_id-1;
}

unsigned int returnUniqueTaskId(int set_id) {
    static volatile int unique_task_id = -1;
    if (set_id == -1){
      return __sync_add_and_fetch(&unique_task_id, 1);
    }
    return unique_task_id = set_id-1;
}


/**
 * \file CPPSs_Task_instance_base.cc
 *
 * \brief source file for the CPPSs_Task_instance_base class
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2012-2014, Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 *
 */

#include "CppSs_Task_instance_base.h"


Task_instance_base::Task_instance_base(CppSs_runtime *rt, int64_t scopeId)
    : /*m_thrd_id(-1),*/
        m_id(returnUniqueTaskId()),
        m_runtime(rt),
        m_priority(0),
        m_predecessor_count(0),
        m_successors({}),
        m_scopeId(scopeId)
{
  //    m_successors.reserve(1000);
}

Task_instance_base::~Task_instance_base()
{
//    LOG(DEBUG1) << "~Task_instance_base " << m_id;
}

void Task_instance_base::add_successor(int64_t task_id)
{
//    std::lock_guard<std::mutex> local_mutex(m_successor_list_mutex);
    m_successors.push_back(task_id);
}

const int64_t Task_instance_base::get_predecessor_count() const
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
#if CPPSS_DEBUG
    if(m_predecessor_count<0){
        LOG(ERROR) << "m_predecessor_count < 0";
    }
#endif
    return m_predecessor_count;
}

const bool Task_instance_base::has_successors() const
{
//    std::lock_guard<std::mutex> local_mutex(m_successor_list_mutex);
    return !m_successors.empty();
}

const std::vector<int64_t> &Task_instance_base::get_successors() const
{
    return m_successors;
}

const int64_t Task_instance_base::increase_predecessor_count()
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
    return __sync_add_and_fetch(&m_predecessor_count,1);
}

const int64_t Task_instance_base::decrease_predecessor_count()
{
//    std::lock_guard<std::mutex> local_mutex(m_predecessor_count_mutex);
    return __sync_sub_and_fetch(&m_predecessor_count,1);
}

#ifndef CPPSS_QUEUE_H
#define CPPSS_QUEUE_H

#include <deque>
#include <cstdint>
#include <mutex>

class CppSs_Queue : private std::deque<int64_t>
{
    public:
        typedef enum {
            width_first,
            depth_first
        } access_policy_t;

        /** Default constructor */
        CppSs_Queue(access_policy_t _access_policy);

        /** Default destructor */
        //virtual ~CppSs_Queue();

        /** Copy constructor
         *  \param other Object to copy from
         */
        CppSs_Queue(const CppSs_Queue& other);

        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        CppSs_Queue& operator=(const CppSs_Queue& other);

        /**
         * \brief add element to queue
         *
         */
        void add_task(const int64_t& task_id);

        /**
         * \brief get element from queue
         * \return false when queue is empty, True otherwise.
         */
        bool get_task(int64_t& task_id);

    protected:
    private:
        const access_policy_t m_access_policy;
        std::mutex m_queue_mutex;
};

#endif // CPPSS_QUEUE_H

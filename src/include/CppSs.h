/*
 * CPPSs.h
 *
 * header file for the CPPSs runtime instance and user functions.
 * To be included in CppSs applications.
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#ifndef CPPSS_H
#define CPPSS_H

#include "CppSs_defines.h"
#include "CppSs_helper.h"
#include "CppSs_profiling.h"
#include "CppSs_logging.h"
#include "CppSs_runtime.h"
#include "CppSs_Task_functor.h"

class Task_instance_base;

namespace CppSs{

#ifdef NO_CPPSS
    const static bool no_CppSs = true;
#else
    const static bool no_CppSs = false;
#endif


    extern CppSs_runtime *runtime;

    extern bool preInitDone;

void PreInit();

    void Init(int n_threads = 2, TLogLevel ll = DEFAULT_REPORTINGLEVEL);

    void Finish();

    /**
     * \brief Halt the thread until task queue is empty
     *
     *
     * \todo
     * do the waiting loop better with
     * \code
     * std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
     * cond.wait(local_mutex,[]{return (runtime->get_task_counter() > 0);});
     * \endcode
     * here and
     * \code
     * std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
     * cond.notify_one();
     * \endcode
     * in dispatch_task?
     * Necessary declaration:
     * \code
     * std::condition_variable cond;
     * \endcode
     * and get rid of
     * \code
     * std::lock_guard<std::mutex> local_mutex(task_counter_mutex);
     * \endcode
     * in runtime->get_task_counter()!
     */
    void Barrier();


    // wrap this factory in a singleton class?
#ifndef NO_CPPSS
    template<typename... ARGS>
      Task_functor<ARGS...> MakeTask(void(*f)(ARGS...),
				     std::initializer_list<dep_t> deps,
				     std::string func_name,
				     int64_t prio=0)
    {
        /// \todo runtime shouild not be initialised here
        if (!preInitDone){
            PreInit();
            LOG(DEBUG1) << "PreInit called by MakeTask().";
        }
        if (runtime == nullptr){
            LOG(ERROR) << "Runtime not present.";
        }

        return Task_functor<ARGS...>(f, deps, func_name, runtime, prio);
    }

#else // #ifndef NO_CPPSS

    template<typename... ARGS>
      std::function<void(ARGS...)> MakeTask(void(*f)(ARGS...),
					    std::initializer_list<dep_t> deps,
					    std::string func_name,
					    int64_t prio=0)
    {
        return f;
    }
#endif // #ifndef NO_CPPSS

}

#endif // CPPSS_H


#ifndef TASK_INSTANCE_BASE_H
#define TASK_INSTANCE_BASE_H

#include "CppSs_includes.h"
#include "CppSs_Task_functor_base.h"

class CppSs_runtime;

class Task_instance_base
{
    public:
        explicit Task_instance_base(CppSs_runtime *rt,
                                    int64_t scopeId=-1);
        virtual ~Task_instance_base();
        void add_successor(int64_t task_id);
//        Task_instance_base(Task_functor_base *f) : m_f(f){}
        const int64_t get_id() const {return m_id;}
        virtual void run(intptr_t thrd_id) const =0;
//        virtual std::string get_func_name() const =0;
        const int64_t get_predecessor_count() const;
        const bool has_successors() const;
        const std::vector<int64_t> &get_successors() const;
        const int64_t increase_predecessor_count();
        const int64_t decrease_predecessor_count();
        const int64_t get_priority() const {return m_priority;}
        void set_priority(const int64_t b){m_priority = b;}
        const int64_t get_scopeId() const {return m_scopeId;}
        //void set_thrd_id(int id);
    protected:
        const int64_t m_id;
        const CppSs_runtime *m_runtime;

//        std::mutex m_successor_list_mutex;
//        std::mutex m_predecessor_count_mutex;

        int64_t m_priority;
        volatile int64_t m_predecessor_count;
        std::vector<int64_t> m_successors;
        // scope, i.e. task which spawned this task
        int64_t m_scopeId;
        //int m_thrd_id;
};


#endif // TASK_INSTANCE_BASE_H

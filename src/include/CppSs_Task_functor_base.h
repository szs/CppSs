/*
 * CPPSs_Task_functor_base.hh
 *
 * header file for the CPPSs Task_functor base class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


#ifndef CPPSS_TASK_FUNCTOR_BASE_H
#define CPPSS_TASK_FUNCTOR_BASE_H

#include "CppSs_defines.h"
#include "CppSs_helper.h"
#include "CppSs_includes.h"
#include "CppSs_runtime.h"

class CppSs_runtime;

class Task_functor_base
{
    public:
        // ctor with automatic, increasing function id
        Task_functor_base (const std::vector<dep_t> deps,
                                    std::string func_name,
                                    CppSs_runtime *rt,
                                    int64_t prio=0);
        // ctor with explicit setting of function id
        Task_functor_base (const std::vector<dep_t> deps,
                                    std::string func_name,
                                    int64_t func_id,
                                    CppSs_runtime *rt,
                                    int64_t prio=0);
        // TODO: copy ctor, default ctor, assignement op
        // dtor
        virtual ~Task_functor_base(){}

        // getters and setters
        const std::string get_func_name() const {return m_func_name;};
        const int64_t get_func_id() const {return m_func_id;};
        const CppSs_runtime *get_runtime () const {return m_runtime;}
        const int64_t get_priority() const {return m_priority;}
        void set_priority(int64_t b){m_priority = b;}


    protected:
        // container holding directionality clauses for each parameter
        std::vector<dep_t> m_deps;
        // container holding type of each parameter. This is only needed for
        // type checking
        std::vector<std::type_info const*> m_types;
        // function name
        const std::string m_func_name;
        // function id
        const int64_t m_func_id;
        // pointer to runtime
        CppSs_runtime *m_runtime;
        // priority level
        int64_t m_priority;
};

#endif // CPPSS_TASK_FUNCTOR_BASE_H

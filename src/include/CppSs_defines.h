#ifndef CPPSS_DEFINES_H
#define CPPSS_DEFINES_H

/*
 * CppSs_defines.hh
 *
 * header file CppSs_defines.h for configuration and convenience defines
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#ifdef HAVE_CONFIG_H
    #include "config.h"
#else
    #define CPPSS_CHECK_TYPES 0
    #define CPPSS_DEBUG 0
    #define CPPSS_TRACING 0
    #define CPPSS_PROFILING 0
    #ifndef USE_AYUDAME
      #define USE_AYUDAME 0
    #endif
#endif

#define CPPSS_NOTASK -1

//#define CPPSS_TASK(x,...) Task_functor cppss_##x((std::function<void (*)(...)>)(x),__VA_ARGS__,#x);

#define PASTE( a, b) a##b
#define CPPSS_TASK(x,...) CppSs::MakeTask(x,__VA_ARGS__,#x);
#define CPPSS_TASKIFY(x,...) auto PASTE(x,_task) = CPPSS_TASK(x,__VA_ARGS__)
#define CPPSS_TASK_PRIORITY(x,...) CppSs::MakeTask(x,__VA_ARGS__,#x,1);
#define CPPSS_TASKIFY_PRIORITY(x,...) auto PASTE(x,_task) = CPPSS_TASK_PRIORITY(x,__VA_ARGS__)

#ifndef DEFAULT_REPORTINGLEVEL
    #define DEFAULT_REPORTINGLEVEL WARNING
#endif

#ifndef USE_DRDOBBS_QUEUE
    #define USE_DRDOBBS_QUEUE 0
#endif



#endif // CPPSS_DEFINES_H

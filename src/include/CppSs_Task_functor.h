/*
 * CPPSs_Task_functor.h
 *
 * header file for the CPPSs Task_functor class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#include "CppSs_defines.h"
#include "CppSs_includes.h"
#include "CppSs_Task_functor_base.h"
#include "CppSs_Task_instance.h"

////////////////////////////////////////////////////////

extern std::mutex CppSs_last_writers_mutex;
extern std::mutex CppSs_new_task_mutex;
//extern std::mutex CppSs_last_readers_mutex;
//extern std::mutex CppSs_last_reductions_mutex;


template<typename... ARGS>
class Task_functor : public Task_functor_base
{
    typedef std::function<void(ARGS...)> f_vararg;

    public:
        // default ctor
        Task_functor() = delete;
        // ctor with automatic generation of function id
        Task_functor(void (*f) (ARGS...),
                              const std::vector<dep_t> deps,
                              std::string func_name,
                              CppSs_runtime *rt,
                              int64_t prio=0);
        // ctor with explicit setting of function id
        Task_functor(void (*f) (ARGS...),
                              const std::vector<dep_t> deps,
                              std::string func_name,
                              int64_t func_id,
                              CppSs_runtime *rt,
                              int64_t prio=0);
        // copy ctor
        Task_functor(const Task_functor<ARGS...> &other);
        // assignement operator
        const Task_functor<ARGS...>& operator=(const Task_functor<ARGS...> &other);
        // dtor
        ~Task_functor(){}
        // ()-operator for calling the task
        void operator()(ARGS... args);

        // Task_instance::run() needs to access the function pointer m_f
        friend void Task_instance<ARGS...>::run(int64_t thrd_id) const;

    private:

        void (*m_f) (ARGS...);
        template <typename... ARG_REST> void get_values(std::vector<void *> *vec, void *value, ARG_REST... rest);
        template <typename... ARG_REST> void get_values(std::vector<void *> *vec, uint64_t value, ARG_REST... rest);
        // initialise value with void for task with empty parameter list
        void get_values(std::vector<void *> *vec, void *value = nullptr);
        void get_values(std::vector<void *> *vec, uint64_t value);
        int64_t new_task(ARGS... args);
        void check_types();
        void check_deps();
};

/*
template<typename... ARGS>
Task_functor<ARGS...>::Task_functor()
: Task_functor_base({},"",nullptr,0), m_f(nullptr)
{
}
*/

template<typename... ARGS>
Task_functor<ARGS...>::Task_functor(void (*f) (ARGS...),
                                    std::vector<dep_t> deps,
                                    std::string func_name,
                                    CppSs_runtime *rt,
                                    int64_t prio)
: Task_functor_base(deps,func_name,rt,prio), m_f(f)
{
    check_deps();

#if CPPSS_CHECK_TYPES
    check_types();
#endif // CPPSS_CHECK_TYPES

    LOG(INFO) << "created task_functor " //<< typeid(this).name()
         << " for function " << m_func_name
         << " with " << function_traits<f_vararg>::nargs << /*" = " << m_types.size() <<*/ " arguments.";
}


template<typename... ARGS>
Task_functor<ARGS...>::Task_functor(void (*f) (ARGS...),
                                    std::vector<dep_t> deps,
                                    std::string func_name,
                                    int64_t func_id,
                                    CppSs_runtime *rt,
                                    int64_t prio)
: Task_functor_base(deps,
                    func_name,
                    func_id,
                    rt,
                    prio),
  m_f(f)
{

    check_deps();

#if CPPSS_CHECK_TYPES
    check_types();
#endif // CPPSS_CHECK_TYPES

    LOG(INFO) << "created task_functor " //<< typeid(this).name()
         << " for function " << m_func_name
         << " with " << function_traits<f_vararg>::nargs << /*" = " << m_types.size() <<*/ " arguments.";
}


template<typename... ARGS>
Task_functor<ARGS...>::Task_functor(const Task_functor<ARGS...> &other)
: Task_functor_base(other.m_deps,
                    other.m_func_name,
                    other.m_func_id,
                    other.m_runtime,
                    other.m_priority),
  m_f(other.m_f)
{
}

template<typename... ARGS>
const Task_functor<ARGS...>& Task_functor<ARGS...>::operator=(const Task_functor<ARGS...> &other)
{
    m_deps = other.m_deps;
    m_func_name = other.m_func_name;
    m_func_id = other.m_func_id;
    m_runtime = other.m_runtime;
    m_priority = other.m_priority;
    return *this;
}

template<typename... ARGS>
int64_t Task_functor<ARGS...>::new_task(ARGS... args)
{
    std::lock_guard<std::mutex> local_mutex(CppSs_new_task_mutex);

    // get scope id
    auto scopeId = m_runtime->get_activity(std::this_thread::get_id());
    // create new task instance
    std::shared_ptr<Task_instance<ARGS...>> task_p(new Task_instance<ARGS...>(this, m_runtime, scopeId));

    if (task_p == nullptr) {
        LOG(ERROR) << "new Task_instance failed!";
    }

    // set the arguments of the task instance
    task_p->set_argument_pack(new std::tuple<ARGS...>( args... )); //TODO: no tuple???

    auto task_id_tmp = task_p->get_id();

    auto task_id = m_runtime->new_task(task_p, scopeId);
    LOG(DEBUG1) << "in new_task: " << task_id << " " << scopeId;

    return task_id; // return task_id
}

template<typename... ARGS>
void Task_functor<ARGS...>::operator()(ARGS... args)
{
#if CPPSS_TRACING
    LOG(TRACE) << "0 2 operator() " << rdtsc() << " 0";
#endif
//    Task_instance<ARGS...> *task(new Task_instance<ARGS...>(this));
//    m_runtime->increase_task_counter();
//    task->set_argument_pack(new std::tuple<ARGS...>( args... ));

    // create new task instance
    int64_t task_id = new_task(args...);

    LOG(DEBUG1) << "in operator() added task " << task_id;

    if (m_runtime->get_task(task_id) == nullptr) {
        LOG(ERROR) << "new_task failed!";
    }

    LOG(DEBUG1) << "AYU_ADDTASK, " << m_func_name << ", taskid: "
                << task_id << " , scopeid:" << m_runtime->get_activity(std::this_thread::get_id());
#if USE_AYUDAME
    int64_t AYU_data[3] = {m_func_id, // function id
                           m_runtime->get_priority(task_id), // priority
                           m_runtime->get_activity(std::this_thread::get_id())}; // scope id

    if (AYU_event) { AYU_event(AYU_ADDTASK, task_id, AYU_data); }

#endif // USE_AYUDAME

    // if the parameter list is empty, there are no dependencies and the task
    // can be queued directly
    if (function_traits<f_vararg>::nargs == 0){
        m_runtime->queue_task(task_id);

#if USE_AYUDAME
        int64_t th_id=0;
        if (AYU_event) { AYU_event(AYU_ADDTASKTOQUEUE, task_id, &th_id); }
#endif

        return;
    }

    std::vector<void *> vec;
    get_values(&vec,args...);

    // is_dependant holds whether task is dependant due to any of its arguments
    // if false after all function arguments have been checked, task is put into
    // global queue
    bool is_dependant = false;

    // dep_tmp holds whether a dependency to a reduction or reader is detected
    // if false, dependency on writer is checked, otherwise not. Only valid
    // during check of a single argument.
    bool dep_tmp = false;

#if CPPSS_TRACING
    LOG(TRACE) << "0 2 operator() " << rdtsc() << " 2";
#endif

    // last_writers and last_readers must be locked for the whole parameter check
    // to avoid race conditions.
    {
        std::lock_guard<std::mutex> local_mutex(CppSs_last_writers_mutex);

        //    CppSs_last_readers_mutex.lock();
        //    CppSs_last_reductions_mutex.lock();
#if CPPSS_TRACING
        LOG(TRACE) << "0 2 operator() " << rdtsc() << " 3";
#endif

        for (size_t i=0; i < function_traits<f_vararg>::nargs; i++) {
            switch (m_deps[i]){
                case IN:
                    // Check whether the memory address in vec is found in last_writers
                    // If so, make the new task instance dependant of the task instance
                    // in last_writers
                    // TODO: memory range instead of start address

    //                LOG(DEBUG3) << "IN " << task_id;

                    // check for reduction
                    dep_tmp = m_runtime->check_dep_on_last_reductions(task_id, vec[i]);

                    if (dep_tmp) {
                        is_dependant = true;
                    } else {
                        // if there is no reduction, check for last writer
                        dep_tmp = m_runtime->check_dep_on_last_writers(task_id, vec[i]);
                        if (dep_tmp) {
                            is_dependant = true;
                        }
                    }

    //                LOG(DEBUG3) << "IN " << task_id << " is_dependant " << is_dependant;

                    // save this task as last reader. Multiple tasks can be (possible) last readers
                    m_runtime->add_to_last_readers(vec[i],task_id);

                break;

                case OUT:
    //                LOG(DEBUG3) << "OUT " << task_id;

                    // check for reduction
                    dep_tmp = m_runtime->check_dep_on_last_reductions(task_id, vec[i]);

                    if (dep_tmp) {
                        is_dependant = true;
                    } else { // if no reductions exist, check for last reader(s)
                        dep_tmp = m_runtime->check_dep_on_last_readers(task_id, vec[i]);
                        if (dep_tmp) {
                            is_dependant = true;
                        }
                    }
    //                LOG(DEBUG3) << "OUT " << task_id << " is_dependant " << is_dependant;

                    // add task to last_writers
                    m_runtime->add_to_last_writers(vec[i], task_id);

                break;

                case INOUT:
    //                LOG(DEBUG3) << "INOUT " << task_id;

                    // check for reduction
                    dep_tmp = m_runtime->check_dep_on_last_reductions(task_id, vec[i]);

                    if (dep_tmp) {
                        is_dependant = true;
                    } else { // if no reductions exist, check for last reader(s)
                        dep_tmp = m_runtime->check_dep_on_last_readers(task_id, vec[i]);
                        if (dep_tmp) {
                            is_dependant = true;
                        } else { // if neither reductions nor last readers are found, check for last writers
                            dep_tmp = m_runtime->check_dep_on_last_writers(task_id, vec[i]);
                            if (dep_tmp) {
                                is_dependant = true;
                            }
                        }
                    }

                    // add task to last_writers
                    m_runtime->add_to_last_writers(vec[i], task_id);

                break;

                case REDUCTION:
    //                LOG(DEBUG3) << "REDUCTION " << task_id;

                    // check for last readers
                    dep_tmp = m_runtime->check_dep_on_last_readers(task_id, vec[i]);
                    if (dep_tmp) {
                        is_dependant = true;
                    } else { // if task is not dependant of a reader, maybe of a last writer
                        dep_tmp = m_runtime->check_dep_on_last_writers(task_id, vec[i]);
                        if (dep_tmp) {
                            is_dependant = true;
                        }
                    }

                    // add task to the last_reductions map
                    m_runtime->add_to_last_reductions(vec[i], task_id);

                break;

                case PARAMETER:
    //                LOG(DEBUG3) << "PARAMETER " << task_id;
                break;
            }
        }

    }
//    CppSs_last_readers_mutex.unlock();
//    CppSs_last_reductions_mutex.unlock();

//    LOG(DEBUG2) << "is_dependant " << is_dependant;
    if (!is_dependant){
#if USE_AYUDAME
        int64_t th_id=0;
        if (AYU_event) { AYU_event(AYU_ADDTASKTOQUEUE, task_id, &th_id); }
#endif
        LOG(DEBUG1) << "Queueing independent task " << task_id;
        m_runtime->queue_task(task_id);
    }
#if CPPSS_TRACING
    LOG(TRACE) << "0 2 operator() " << rdtsc() << " 9";
#endif
}

template<typename... ARGS>
void Task_functor<ARGS...>::check_types()
{
    LOG(DEBUG1) << "Check types";
    get_types<f_vararg>(m_types);

    for (size_t i=0; i < function_traits<f_vararg>::nargs; ++i) {
        if ( m_types[i] == &typeid(int*) ){
            LOG(DEBUG1) << "int*";
            LOG(WARNING) << "In function " << m_func_name << ": \n\tUse int64_t* or alike to assure consistent accuracy across platforms!";
            if ( m_deps[i] == PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tPARAMETER must not be a pointer type!";
            }
        } else if
        ( m_types[i] == &typeid(unsigned int*) ){
            LOG(DEBUG1) << "unsigned int*";
            LOG(WARNING) << "In function " << m_func_name << ": \n\tUse uint64_t* or alike to assure consistent accuracy across platforms!";
            if ( m_deps[i] == PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tPARAMETER must not be a pointer type!";
            }
        } else if
        ( m_types[i] == &typeid(double*) ){
            LOG(DEBUG1) << "double*";
            if ( m_deps[i] == PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tPARAMETER must not be a pointer type!";
            }
        } else if
        ( m_types[i] == &typeid(float*) ){
            LOG(DEBUG1) << "float*";
            if ( m_deps[i] == PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tPARAMETER must not be a pointer type!";
            }
        } else if
        ( m_types[i] == &typeid(double) ){
            LOG(DEBUG1) << "double";
            if ( m_deps[i] != PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tNon pointer types only allowed with PARAMETER!";
            }
        } else if
        ( m_types[i] == &typeid(float) ){
            LOG(DEBUG1) << "float";
            if ( m_deps[i] != PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tNon pointer types only allowed with PARAMETER!";
            }
        } else if
        ( m_types[i] == &typeid(int) ){
            LOG(DEBUG1) << "int";
            if ( m_deps[i] != PARAMETER) {
                LOG(ERROR) << "In function " << m_func_name << ": \n\tNon pointer types only allowed with PARAMETER!";
            }
        } else {
            LOG(ERROR) << "In function " << m_func_name << ": \n\tUnsupported data type.";
        }
    }
}

template<typename... ARGS>
void Task_functor<ARGS...>::check_deps()
{

    if (sizeof...(ARGS) != m_deps.size() ){
        LOG(ERROR) << "in task " << m_func_name << " found " << sizeof...(ARGS)
                   << " arguments, but only " << m_deps.size()
                   << " directionality identifiers.";
    }
}

template<typename... ARGS>
void Task_functor<ARGS...>::get_values(std::vector<void *> *vec, void *value)
{
    vec->push_back(value);
}

template<typename... ARGS>
void Task_functor<ARGS...>::get_values(std::vector<void *> *vec, uint64_t value)
{
    vec->push_back(nullptr);
}

template<typename... ARGS>
template<typename... ARG_REST>
void Task_functor<ARGS...>::get_values(std::vector<void *> *vec, void *value, ARG_REST... rest)
{
    vec->push_back(value);
    get_values(vec,rest...);
}

template<typename... ARGS>
template<typename... ARG_REST>
void Task_functor<ARGS...>::get_values(std::vector<void *> *vec, uint64_t value, ARG_REST... rest)
{
    vec->push_back(nullptr);
    get_values(vec,rest...);
}

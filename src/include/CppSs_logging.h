/*
 * CppSs_logging.h
 *
 * header file for logging
 * from http://drdobbs.com/article/print?articleID=201804215
 *
 *
 *
 */

#ifndef CPPSS_LOGGING_H
#define CPPSS_LOGGING_H

#include <sstream>
#include <string>
#include <stdio.h>
#include <mutex>


inline std::string NowTime();

enum TLogLevel {QUIET, ERROR, TRACE, WARNING, INFO, DEBUG1, DEBUG2, DEBUG3, DEBUG4};

class Log
{
public:
    Log();
    virtual ~Log();
    std::ostringstream& Get(TLogLevel level = INFO);
public:
    static TLogLevel& ReportingLevel();
    static std::string ToString(TLogLevel level);
    static TLogLevel FromString(const std::string& level);
protected:
    std::ostringstream os;
private:
    Log(const Log&);
    Log& operator =(const Log&);
    std::mutex logging_mutex;

};

inline Log::Log()
{
}

inline std::ostringstream& Log::Get(TLogLevel level)
{
    std::lock_guard<std::mutex> local_mutex(logging_mutex);
    os << "\033[0mCppSs - " << NowTime();
    os << " " << ToString(level) << ": ";
    os << std::string(level > DEBUG1 ? level - DEBUG1 : 0, '\t');
    return os;
}

inline Log::~Log()
{
    std::lock_guard<std::mutex> local_mutex(logging_mutex);
    os << std::endl;
    fprintf(stderr, "%s\033[0m", os.str().c_str());
    fflush(stderr);
}

inline TLogLevel& Log::ReportingLevel()
{
    static TLogLevel reportingLevel = DEBUG4;
    return reportingLevel;
}

/*
\033[22;30m - black
\033[22;31m - red
\033[22;32m - green
\033[22;33m - brown
\033[22;34m - blue
\033[22;35m - magenta
\033[22;36m - cyan
\033[22;37m - gray
\033[01;30m - dark gray
\033[01;31m - light red
\033[01;32m - light green
\033[01;33m - yellow
\033[01;34m - light blue
\033[01;35m - light magenta
\033[01;36m - light cyan
\033[01;37m - white
*/
inline std::string Log::ToString(TLogLevel level)
{
	static const char* const buffer[] = {"QUIET",
                                         "\033[01;31mERROR",
                                         "\033[0mTRACE",
                                         "\033[01;33mWARNING",
                                         "\033[0mINFO",
                                         "\033[0mDEBUG1",
                                         "\033[0mDEBUG2",
                                         "\033[0mDEBUG3",
                                         "\033[0mDEBUG4"};
    return buffer[level];
}

inline TLogLevel Log::FromString(const std::string& level)
{
    if (level == "QUIET")
        return QUIET;
    if (level == "TRACE")
        return TRACE;
    if (level == "ERROR")
        return ERROR;
    if (level == "WARNING")
        return WARNING;
    if (level == "INFO")
        return INFO;
    if (level == "DEBUG1")
        return DEBUG1;
    if (level == "DEBUG2")
        return DEBUG2;
    if (level == "DEBUG3")
        return DEBUG3;
    if (level == "DEBUG4")
        return DEBUG4;
    Log().Get(WARNING) << "Unknown logging level '" << level << "'. Using INFO level as default.";
    return INFO;
}

typedef Log Log_t;

#define LOG(level) \
    if (level > Log_t::ReportingLevel()) ; \
    else Log().Get(level)


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)

#include <windows.h>

inline std::string NowTime()
{
    const int MAX_LEN = 200;
    char buffer[MAX_LEN];
    if (GetTimeFormatA(LOCALE_USER_DEFAULT, 0, 0,
            "HH':'mm':'ss", buffer, MAX_LEN) == 0)
        return "Error in NowTime()";

    char result[100] = {0};
    static DWORD first = GetTickCount();
    sprintf(result, "%s.%03ld", buffer, (long)(GetTickCount() - first) % 1000);
    return result;
}

#else

#include <sys/time.h>

inline std::string NowTime()
{
    char buffer[11];
    time_t t;
    time(&t);
    tm r = {0};
    strftime(buffer, sizeof(buffer), "%X", localtime_r(&t, &r));
    struct timeval tv;
    gettimeofday(&tv, 0);
    char result[100] = {0};
    sprintf(result, "%s.%03ld", buffer, (long)tv.tv_usec / 1000);
    return result;
}

#endif //WIN32

#endif // CPPSS_LOGGING_H

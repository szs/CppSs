/*
 * CPPSs_runtime.hh
 *
 * header file for the CPPSs_runtime class
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */


#ifndef CPPSS_RUNTIME_H
#define CPPSS_RUNTIME_H

//#include "CppSs_helper.h"
#include "CppSs_profiling.h"
#include "CppSs_includes.h"
#include "CppSs_Task_instance_base.h"

#if USE_DRDOBBS_QUEUE
    #include "CppSs_LowLockQueue.h"
#else
    #include "CppSs_Queue.h"
#endif // USE_DRDOBBS_QUEUE

class CppSs_runtime
{
    public:
        CppSs_runtime();
        ~CppSs_runtime();

        void add_worker(int64_t n_worker);
        const int get_n_worker() const {return m_worker.size();};
        const int get_n_threads() const {return m_worker.size()+1;};
        void stop_all_threads();
        const bool should_stop_thread(intptr_t i) const;

        const int64_t new_task(std::shared_ptr<Task_instance_base> task_p, int64_t scopeId=-1);
        void run_task(int64_t task_id, CppSs_runtime *runtime, int64_t i);
        const std::shared_ptr<Task_instance_base> get_task(int64_t task_id) const;
        void enqueue_free_successors(int64_t task_id);
        const int64_t get_priority(int64_t task_id) const;

        const bool add_to_last_writers(void *v, int64_t task_id);
        void remove_task_from_last_writers(int64_t task_id);

        void add_to_last_readers(void *v, int64_t task_id);
        void remove_task_from_last_readers(int64_t task_id);

        void add_to_last_reductions(void *v, int64_t task_id);
        void remove_task_from_last_reductions(int64_t task_id);

        const bool check_dep_on_last_reductions(int64_t task_id, void *v);
        const bool check_dep_on_last_writers(int64_t task_id, void *v) const;
        const bool check_dep_on_last_readers(int64_t task_id, void *v);

        void queue_task(const int64_t task_id);
        bool dequeue_task( int64_t* task_id );

        const int64_t get_n_tasks() const {return m_tasks.size();};
        //const int64_t get_n_global_queue() const {return m_global_queue.size();};
        const int64_t get_n_unfinished_tasks(int64_t scopeId) const;
        const int64_t get_n_total_tasks() const {return m_total_tasks_counter;};

        // access thread activity vector
        int64_t set_activity(std::thread::id thread_id, int64_t task_id);
        int64_t get_activity(std::thread::id thread_id) const;

#if CPPSS_DEBUG
        void print_all_tasks() const;
#endif

    private:
        static void dispatch_task(int64_t thread_id, CppSs_runtime *rt);
        void increase_unfinished_tasks_counter(int64_t scopeId);
        void decrease_unfinished_tasks_counter(int64_t scopeId);
        void increase_total_tasks_counter();
        //void decrease_total_tasks_counter();

        std::map<int64_t, uint64_t> m_unfinished_tasks_counter; ///< unfinished tasks per scope
        volatile int64_t m_total_tasks_counter;

        std::map< void*, int64_t > m_last_writers;
        std::multimap< void*, int64_t > m_last_readers;
        std::multimap< void*, int64_t > m_last_reductions;

#if USE_DRDOBBS_QUEUE
        LowLockQueue<int64_t> m_global_queue;
        LowLockQueue<int64_t> m_priority_queue;
#else
        CppSs_Queue m_global_queue;
        CppSs_Queue m_priority_queue;
#endif // USE_DRDOBBS_QUEUE

        std::vector<std::shared_ptr<Task_instance_base>> m_tasks;

        std::vector<bool> m_stop_thread_vec;
        // thread activity map stores which task is executed momentarily
        // by each thread. CPPSS_NOTASK, if none.
        std::map<std::thread::id, int64_t> m_thread_activity;
        std::vector<std::shared_ptr<std::thread>> m_worker;
};

//inline void CppSs_runtime::increase_task_counter()
//{
//    __sync_add_and_fetch(&m_task_counter,1);
//}
//
//inline void CppSs_runtime::decrease_task_counter()
//{
//    __sync_sub_and_fetch(&m_task_counter,1);
//}


#endif // CPPSS_RUNTIME_H

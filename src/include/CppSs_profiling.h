#ifndef CPPSS_PROFILING_H
#define CPPSS_PROFILING_H

/*
 * CppSs_profiling.h
 *
 * header file CppSs_profiling.h for profiling capabilities
 *
 * (C) 2012, Steffen Brinkmann, GPL
 *
 */

#include "CppSs_includes.h"
#include <atomic>
#include <memory.h>

namespace CppSs_Profiling{

  /* vectors for clocks */
  extern std::vector<timespec> time_vec;
  extern std::vector<timespec> timetmp_vec;
  extern std::vector<std::string> clockname_vec;
  extern std::vector<clockid_t> cid_vec;
  extern std::vector<bool> running_vec;

  /* clock functions */
  void start_clock(unsigned id, std::string s, clockid_t cid=CLOCK_REALTIME);
  void restart_clock(unsigned id);
  void stop_clock(unsigned id);
  void reset_clock(unsigned id);

  /* vectors for counters */
  extern std::vector<unsigned> counter_vec;
  extern std::vector<std::string> countername_vec;

  /* counter functions */
  void init_counter(unsigned id, std::string s);
  void increase_counter(unsigned id);
  void decrease_counter(unsigned id);
  void reset_counter(unsigned id);

  /* report */
  std::string report();



}

#endif // CPPSS_PROFILING_H

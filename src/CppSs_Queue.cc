/**
 * \file CppSs_Queue.cc
 *
 * \brief implementation file for the CppSs queue
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 *
 * \copyright
 * (C) 2012-2014, Steffen Brinkmann.\n
 * This software is published under the terms of the BSD license.
 *
 */

#include "CppSs_Queue.h"

CppSs_Queue::CppSs_Queue(access_policy_t _access_policy)
    : m_access_policy(_access_policy)
{
    //ctor
}

/*
CppSs_Queue::~CppSs_Queue()
{
    //dtor
}
*/

CppSs_Queue::CppSs_Queue(const CppSs_Queue& other)
    : m_access_policy(other.m_access_policy)
{
    //copy ctor
}

CppSs_Queue& CppSs_Queue::operator=(const CppSs_Queue& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}


void CppSs_Queue::add_task(const int64_t& task_id)
{
    std::lock_guard<std::mutex> local_mutex(m_queue_mutex);
    push_back(task_id);
}

bool CppSs_Queue::get_task(int64_t& task_id)
{
    std::lock_guard<std::mutex> local_mutex(m_queue_mutex);
    if (empty()) return false;
    int64_t result;
    if (m_access_policy == depth_first){
        task_id = back();
        pop_back();
    }
    else if (m_access_policy == width_first){
        task_id = front();
        pop_front();
    }
    return true;
}

#include <CppSs.h>
#include <iostream>

void f_out(int *a){std::cout << "f_out" << std::endl;}
CPPSS_TASKIFY(f_out,{OUT})
void f_inout(int *a){std::cout << "f_inout" << std::endl;}
CPPSS_TASKIFY(f_inout,{INOUT})
void f_in(int *a){std::cout << "f_in" << std::endl;}
CPPSS_TASKIFY(f_in,{IN})

int main (void)
{
  unsigned N = 3;
  int *a = new int[N];
  
  CppSs::Init(2);
  for (unsigned i=0; i < N; ++i){
    f_out_task(&a[i]);
    f_inout_task(&a[i]);
    f_in_task(&a[i]);
  }
  CppSs::Finish();
  delete[] a;
}
